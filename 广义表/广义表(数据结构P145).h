#include<stdio.h>
#include<stdlib.h>
#define MAXSIZE 100
#define OK 1
#define ERROR 0
#define ATOM 0
#define LIST 1
typedef enum {ATOM, LIST} ElemTag;

//广义表的头尾链表存储结构类型定义如下： 
typedef struct GLNode{
	ElemTag tag;
	union{
		char c;
		struct {
			struct GLNode *hp, *tp;
		}htp;
	}atom_htp;
}GLNode, *GList;

//广义表的同层结点链存储结构
 
//typedef struct GLNode{
//	ElemTag tag;
//	union{
//		char c;
//		struct GLNode *hp;
//	}atom_hp;
//	struct GLNode *tp;
//}GLNode, *GList;

//广义表的操作实现
GList Head(GList L)
{
	//求广义表 L 的表头, 并返回表头指针
	if(L == NULL) return NULL;
	if(L->tag == ATOM) exit(0);
	else return (L->atom_htp.htp.hp); 
}

//求广义表的表尾
GList Tail(GList L)
{
	//求广义表 L 的表尾, 并返回表尾指针
	if(L == NULL) return NULL;
	if(L->tag == ATOM) exit(0);
	else return (L->atom_htp.htp.tp); 
}

//求广义表的长度
int Length(GList L)
{
	//求广义表 L 的长度, 并返回长度值
	int k = 0;
	GLNode *s;
	if(L == NULL) return 0;
	if(L->tag == ATOM) exit(0);
	s = L;
	while(s != NULL)
	{
		k++;
		s = s->atom_htp.htp.tp;
	}
	return k;
}

//求广义表的深度
int Depth(GList L)
{
	//求广义表 L 的深度, 并返回深度值
	int d, max;
	GLNode *s;
	if(L == NULL) return 1;
	if(L->tag == ATOM) return 0;
	s = L;
	while(s != NULL)
	{
		d = Depth(s->atom_htp.htp.hp);
		if(d > max) max = d;
		s = s->atom_htp.htp.tp;
	}
	return max + 1;
}

//统计广义表中原子的数目
int CountAtom(GList L)
{
	//求广义表 L 中原子结点数目, 并返回原子结点数目值
	int n1, n2;
	if(L == NULL) return 0;
	if(L->tag == ATOM) return 1;
	n1 = CountAtom(L->atom_htp.htp.hp);
	n2 = CountAtom(L->atom_htp.htp.tp);
	return n1 + n2; 
}

//复制广义表
int CopyGList(GList S, GList *T)
{
	if(S == NULL)
	{
		*T = NULL;
		return OK;
	}
	*T = (GLNode *)malloc(sizeof(GLNode));
	if(*T == NULL) return ERROR;
	(*T)->tag = S->tag;
	if(S->tag == ATOM) (*T)->atom = S->atom;
	else
	{
		CopyGList(S->atom_htp.htp.hp, &((*T)->atom_htp.htp.hp));//复制表头 
		CopyGList(S->atom_htp.htp.tp, &((*T)->atom_htp.htp.tp));//复制表尾 
	}
	return OK;
}
