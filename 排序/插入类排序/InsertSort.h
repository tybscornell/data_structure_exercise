#include<stdio.h>
#include<stdlib.h>

typedef int TypeElem;
typedef int OtherType;

//定义数据类型 
typedef struct{
	TypeElem data;
	OtherType other_data;
}RecordType;


//插入类排序方法 
//直接插入排序
void insSort(RecordType array[], int length)
{
	//将第一个至最后一个元素依次插入到有序的数组中 
	for(int i = 1; i < length; i++)
	{
		//记录待插入元素的值 
		RecordType temp = array[i];
		//从待插元素的前一个元素进行比较 
		int j = i-1;
		//若数组未越界且待插元素持续小于前边元素, 则边移动边查找 
		for(; j >= 0 && array[j].data > temp.data; j--)
		{
			array[j+1] = array[j];
		}
		//查找成功时或者已经移至边界, 将元素插入数组腾出的待插位置中 
		array[j+1] = temp;
	}
	//直接插入发插入完毕 
}


//采用折半插入排序
void binSort(RecordType array[], int length)
{
	//将第一个数至最后一个数顺序插入至数组中 
	for(int i = 1; i < length; i++)
	{
		//定义临时变量存储待插入的值 
		RecordType temp = array[i];
		//定义折半法查找的开始与结束位置,中间位置mid, 和变量 j 
		int low = 0, high = i-1, mid, j;
		//当 low <= high时循环查找插入位置,当low > high时,low即为插入位置 
		while(low <= high)
		{
			mid = (low+high)/2;
			//当array[mid].data > temp.data时, 查找上界缩小到 mid-1 
			if(array[mid].data > temp.data) high = mid-1;
			//否则查找下界缩短到 mid+1 
			else low = mid+1;
		}
		//插入位置后的元素顺次移动 
		for(j = i-1; j >= low; j--)
		array[j+1] = array[j];
		//将待插元素插入到数组中 
		array[low] = temp;
	}
	//折半法插入排序成功 
}


//采用插入类排序希尔排序进行排序
void shellSort(RecordType array[], int length)
{
	int sum = 0, count = length;
	//获取子序列元素相隔距离 d的数组长度 
	while(count)
	{
		sum++;
		count /= 2;
	}
	//定义子序列元素相隔距离数组delta[],变量i,j 
	int delta[sum], i, j;
	//将子序列元素之间相隔的距离依次添加到距离数组中 
	for(i = 0, count = length/2; i < sum; i++, count /= 2)
	{
		delta[i] = count;
	}
	
	//进行希尔排序
	//对数组进行 sum 趟希尔排序 
	for(i = 0; i < sum; i++)
	{
		//从第一个子序列的第二个记录开始,各个子序列记录轮流出现,进行插入排序 
	    for(j = 0+delta[i]; j < length; j++)
		{
			//记录当前插入元素 
			RecordType temp = array[j];
			//从当前插入元素所在子序列的上个元素进行比较插入 
			int k = j-delta[i];
			//对子序列中的元素进行边比较边移动 
			for(; k >= 0 && array[k].data > temp.data; k -= delta[i])
			array[k+delta[i]] = array[k];
			//当数组到达边界或者成功找到插入位置, 将此元素插入 
			array[k+delta[i]] = temp;
		}
		//一趟希尔排序完成 
    }
    //总的希尔排序完成 
}


//打印数组 
void printArray(RecordType array[], int length)
{
	for(int i = 0; i < length; i++)
	{
		printf("%d\t", array[i].data);
	}
	printf("\n");
}
