#include<stdio.h>
//#include"InsertSort.h"
//#include"SwapSort.h"
#include"ChooseSort.h"
#define N 200000
/*
测试:
当数组中存在200000个完全逆序的数据时
采用希尔插入排序: 3.0s完成排序(不稳定) 
采用折半插入排序: 107.0s完成排序(稳定) 
采用直接插入排序: 114.0s完成排序(稳定)
*/
int main()
{
	RecordType array[N];
	int n;
	printf("Please enter length of arrays:\n");
	scanf("%d", &n);
	for(int i = 0; i < n; i++)
	{
        array[i].data = n-i-1;
	}
	printf("Unsorted array:\n");
	printArray(array, n);
	selectSort(array, n);
//	bubbleSort(array, n);
//	qkSort(array, 0, n-1);
//	insSort(array, n);
//  binSort(array, n);
//	shellSort(array, n);
	printf("Sorted array:\n");
	printArray(array, n);
	return 0;
}
