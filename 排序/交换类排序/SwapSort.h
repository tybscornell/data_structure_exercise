#include<stdio.h>
#include<stdlib.h>

typedef int TypeElem;
typedef int OtherType;

//定义数据类型 
typedef struct{
	TypeElem data;
	OtherType other_data;
}RecordType;


//交换类排序方法 
//冒泡法排序
void bubbleSort(RecordType array[], int length)
{
	//定义交换标志 flag初始化为 1 
	int flag = 1;
	//通过length-1次比较 , 每次循环将最大值挪至数组最后 
	for(int i = 0; i < length-1 && flag; i++)
	{
		//定义交换标志,若存在一次未交换, 代表数组完成排序,跳出循环 
		flag = 0;
		//通过两两值得比较将此次循环的最大值挪至最后 
		for(int j = 0; j < length-i-1; j++)
		{
			//若数组前一个元素的值大于后一个元素的值 
			if(array[j].data > array[j+1].data)
			{
				//进行交换 
				RecordType temp = array[j];
				array[j] = array[j+1];
				array[j+1] = temp;
				//交换标志置为1 
				flag = 1;
			}
		}
	}
}



//基于交换类排序的快速排序函数, low为数组下标起始位置, high为数组下标结束位置 
//递归对子数组进行排序 
void qkSort(RecordType array[], int low, int high)
{
	//若数组开始位置小于结束位置时开始排序 
	if(low < high)
	{
		//记录本次的开始位置和结束位置并赋值给start和end变量 
		int start = low, end = high;
		//记录下此枢纽为 temp 
		RecordType temp = array[start];
		//当start < end时持续进行交换 
		while(start < end)
		{ 
            //end指向数组末尾,当array[end].data > temp.data时且start<end时 
			while(start < end && array[end].data >= temp.data)
			end--;			//end累减
			//若start<end说明array[end].data<temp.data,此时 
			if(start < end)
			{
				//将array[end]赋值给array[start]相当于将小于temp的数前移 
				array[start] = array[end];
				//start累加 
				start++;
			}
			//start指向数组开头左右,当array[end].data < temp.data时且start<end时 
			while(start < end && array[start].data < temp.data)
			start++;		//start累加 
			//若start<end说明array[end].data>=temp.data,此时 
			if(start < end)
			{
				//将array[start]赋值给array[end]相当于将大于temp的数后移 
				array[end] = array[start];
				//end累减 
				end--;
			}
		}
		//start和end相等时, 把枢纽放到分界处,此位置前元素小于此枢纽,后面的数大于此枢纽 
		array[start] = temp;
		//继续对左子数组进行排序 
		qkSort(array, low, start-1);
		//继续对右子数组进行排序 
		qkSort(array, start+1, high);
	}
}


//打印数组 
void printArray(RecordType array[], int length)
{
	for(int i = 0; i < length; i++)
	{
		printf("%d\t", array[i].data);
	}
	printf("\n");
}

