#include<stdio.h>


//折半法查找, 数组必须为有序递增的, n代表数组长度, data代表要查找的数字 
int harfSearch(int[] array, int n, int data)
{
	int low = 0, high = n-1, mid;
	while(low <= high)
	{
		mid = (low + high)/2;
		if(array[mid] == data)
		    return mid+1;
		else if(array[mid] > data)
		    high = mid-1;
        else
            low = mid+1;
	}
	//未找到返回 0 
	return 0;
}


//顺序查找法, 数组可以无序, n代表数组长度, data代表要查找的数字 
int orderSearch(int[] array, int n, int data)
{
	for(int i = 0; i < n; i++)
	    if(array[i] == data)
	        return i+1;		//返回查找到第一个元素的序号 

    //未找到则返回 0 
    return 0;
}
