#include<stdio.h>
#include<stdlib.h>
#define LEN sizeof(Node)
#define OK 1
#define ERROR 0
#define TRUE 1
#define FALSE 0

typedef int TypeElem;

typedef struct Node{
	TypeElem data;
	Node *left;			//定义二叉排序树的左子树 
	Node *right;		//定义二叉排序树的右子树 
}*SortTree;



//向二叉排序树中插入结点
SortTree insertBST(SortTree root, TypeElem data)
{
	//若根为空时 
	if(root == NULL)
	{
		root = (SortTree)malloc(LEN);	//开辟新的树
		root->data = data;				//将添加的数据赋值到此根 
		root->left = NULL;				//此树的左子树为NULL 
		root->right = NULL;				//此树的右子树为NULL 
		return root;
	}
	//若插入的数值小于根节点的数值, 则将此数插入到左子树 
	if(root->data > data) root->left = insertBST(root->left, data);
	//若插入的数值大于根节点的数值, 则将此数插入到右子树 
	if(root->data < data) root->right = insertBST(root->right, data);
	//返回开辟的根 
	return root;
}


//创建二叉排序树, 即可以一次多个进行二叉排序树的插入 
SortTree createBST(SortTree root)
{
	TypeElem data;
	printf("输入一些数字进行插入创建二叉排序树(以其它字符结束):\n");
	//循环输入结点的值进行插入 
	while(scanf("%d", &data) == 1)
	{
		//插入操作 
		root = insertBST(root, data);
	}
	//返回根节点 
	return root;
}


//二叉排序树查找的递归算法
SortTree searchBST(SortTree root, TypeElem data)
{
    if(root == NULL) return NULL;				//若此树为空, 查找失败, 返回 NULL 
	//若树根的结点等于要查找的数据, 返回此树根
	if(root->data == data) return root;
	//若树根的结点大于要查找的数据, 递归查找左子树 
	else if(root->data > data) return searchBST(root->left, data);
	//若树根的结点小于要查找的数据, 递归查找右子树 
	else return searchBST(root->right, data);
}


//二叉排序树的非递归算法
SortTree searchCycleBST(SortTree root, TypeElem data)
{
	//定义二叉排序树临时遍历结点 p 
    SortTree p = root;
	while(p != NULL)
	{
		//若树根的结点等于要查找的数据, 跳出循环, 返回树根 
	    if(p->data == data) break;
	    //若树根的结点大于要查找的数据, 查询此树的左子树 
		else if(p->data > data) p = p->left;
		//若树根的结点小于要查找的数据, 查询此树的右子树 
		else p = p->right;	
	}
	//返回查询的结果 
	return p;
}


//删除二叉排序树中的一个结点 
SortTree deleteBST(SortTree root, TypeElem data)
{
	SortTree q = root, p = NULL;
	//查找要删除的结点在二叉排序树上的位置 
	while(q != NULL)
	{
		if(q->data == data) break;
		p = q;
		if(q->data < data) q = q->right;
		else q = q->left;
	}
	//如果未找到此结点或此二叉排序树为空, 返回 root 
	if(q == NULL) return root;
	//若此树仅有一个结点, 删除此结点并返回空 
	if(p == NULL)
	{
		free(q);
		return NULL;
	}
	//若成功找到二叉排序树中的这个结点并且是其双亲结点的左孩子 
	if(q == p->left)
	{
		//若此结点为叶子结点, 直接删除此结点 
		if(q->left == NULL && q->right == NULL)
		{
			p->left == NULL;
			free(q);
		}
		//结点的左右孩子有一个为空, 则将此结点孩子连接至此结点双亲结点 
		else if(q->left == NULL || q->right == NULL)
		{
			//左孩子结点不为空, 将此结点的左孩子连接至其双亲结点 
			if(q->left != NULL)
			{
				p->left = q->left;
				free(q);
			}
			//右孩子结点不为空, 将此结点的右孩子连接至其双亲结点 
			else
			{
				p->left = q->right;
				free(q);
			}
		}
		//此结点均有左右孩子, 找到此结点的中序前驱结点,左孩子连接其双亲结点, 右孩子连接至其前驱结点的右孩子 
		else
		{
			SortTree s = q->left;
			//找到此结点的中序前驱结点
			while(s->right != NULL)
			{
				s = s->right;
			}
			//进行连接 
			p->left = q->left;
			s->right = q->right;
			free(q);
		}
	}
	//若成功找到二叉排序树中的这个结点并且是其双亲结点的右孩子 
	else
	{
		//若此结点为叶子结点, 直接删除此结点 
		if(q->left == NULL && q->right == NULL)
		{
			p->right == NULL;
			free(q);
		}
		//结点的左右孩子有一个为空, 则将此结点孩子连接至此结点双亲结点 
		else if(q->left == NULL || q->right == NULL)
		{
			//左孩子结点不为空, 将此结点的左孩子连接至其双亲结点
			if(q->left != NULL)
			{
				p->right = q->left;
				free(q);
			}
			//右孩子结点不为空, 将此结点的左孩子连接至其双亲结点
			else
			{
				p->right = q->right;
				free(q);
			}
		}
		//此结点均有左右孩子, 找到此结点的中序前驱结点,左孩子连接其双亲结点, 右孩子连接至其前驱结点的右孩子
		else
		{
			SortTree s = q->left;
			//找到此结点的中序前驱结点
			while(s->right != NULL)
			{
				s = s->right;
			}
			//进行连接 
			p->right = q->left;
			s->right = q->right;
			free(q);
		}
	}
	return root;
}


//中序遍历二叉排序树 
void inOrder(SortTree root)
{
	if(root == NULL) return;		//若树为空, 返回 
	inOrder(root->left);			//遍历树的左子树 
	printf("%d\n", root->data);		//访问数据 
	inOrder(root->right);			//遍历树的右子树 
	return;
}
