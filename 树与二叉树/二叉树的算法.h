#include<stdio.h>
#include<stdlib.h>
#include<String.h>
#define LEN sizeof(Node)

typedef char TypeElem;
int leafCount = 0;
int depth = 0;

typedef struct Node{
	TypeElem data;			//数据 
	struct Node *LChild;	//左子树 
	struct Node *RChild;	//右子树 
}BiTNode, *BiTree;

BiTree createBiTree01(BiTree bt);	//先序创建二叉树 
BiTree createBiTree02(BiTree bt);	//中序创建二叉树 
BiTree createBiTree03(BiTree bt);	//后序创建二叉树
BiTree createBiTreePI(BiTree bt, char str1[], char str2[]);	//根据树的先序和中序创建树 
BiTree createBiTreeIP(BiTree bt, char str1[], char str2[]);	//根据树的中序和后序创建树 
void prePrintNode(BiTree bt);	//先序输出结点 
void inPrintNode(BiTree bt);	//中序输出结点 
void postPrintNode(BiTree bt);	//后序输出结点 
int preGetCount(BiTree bt);		//先序统计结点个数 
int inGetCount(BiTree bt);		//中序统计结点个数 
int postGetCount(BiTree bt);	//后序统计结点个数 
void prePrintLeaf(BiTree bt);	//先序输出叶子结点 
void inPrintLeaf(BiTree bt);	//中序输出叶子结点 
void postPrintLeaf(BiTree bt);	//后序输出叶子结点 
void leafCount01(BiTree bt);	//先序统计叶子结点 
void leafCount02(BiTree bt);	//中序统计叶子结点 
void leafCount03(BiTree bt);	//后序统计叶子结点 
int leaf(BiTree bt);		//分治算法统计叶子结点 
int postTreeDepth(BiTree bt);	//后序求二叉树的高度 
void preTreeDepth(BiTree bt, int h);	//先序求二叉树的高度 
void PrintTree(BiTree bt, int nLayer);	//逆时针旋转 90 度打印二叉树 


//创建二叉树 
//采用先序创建二叉树 
BiTree createBiTree01(BiTree bt)
{
	char ch;
	ch = getchar();
	if(ch == '.')
	bt = NULL;	//若输入数据为 . ,表示不创建此子树 
	else
	{	//否则开辟子树空间 
		bt = (Node *)malloc(LEN);
	    bt->data = ch;	//输入根数据 
	    bt->LChild = createBiTree01(bt->LChild);	
	    bt->RChild = createBiTree01(bt->RChild);	//创建左右子树 
	}
	return bt;
}

//采用中序创建二叉树 
BiTree createBiTree02(BiTree bt)
{
	char ch;
	ch = getchar();
	if(ch == '.')
	bt = NULL;
	else
	{
		bt = (Node *)malloc(LEN);
		bt->LChild = createBiTree02(bt->LChild);	//优先创建左子树 
		bt->data = ch;	//读入根数据 
		bt->RChild = createBiTree02(bt->RChild);	//创建右子树 
	}
	return bt;
}

//采用后序创建二叉树
BiTree createBiTree03(BiTree bt)
{
	char ch;
	ch = getchar();
	if(ch == '.')
	bt = NULL;
	else
	{
		bt = (Node *)malloc(LEN);
		bt->LChild = createBiTree03(bt->LChild);	//创建左子树 
		bt->RChild = createBiTree03(bt->RChild);	//创建右子树
		bt->data = ch; //最后读入根数据 
	}
	return bt;
}



//根据树的先序和中序创建树 
BiTree createBiTreePI(BiTree bt, char str1[], char str2[]) //根据树的先序和中序创建树 
{
	//采用递归算法 
	int index;
	bt = (Node *)malloc(LEN);
	bt->data = str1[0];		//首先读入序列 str1 的首字母, 为树根 
	if(strlen(str1) == 1)	//若串 的长度为 1, 说明其无左右子串 
	{
		bt->LChild = NULL;
		bt->RChild = NULL;
		return bt;
	}
	for(int i = 0; i < strlen(str2); i++)
    {
    	if(str2[i] == str1[0])
	    {
	    	index = i;
	    	break;
		}
	}						//在串 B 中查找 串A首字符 所在的位置 index 
	if(index == 0)	
	{
        //若 index 为 0, 表示此树仅有右子树 , 无左子树 
		int i, j;
		char str01[strlen(str1)-1] = {'\0'};
		char str02[strlen(str2)-1] = {'\0'};
		for(i = 0, j = 1; i < strlen(str1)-1; i++, j++)
		{
			str01[i] = str1[j];
			str02[i] = str2[j];			//开辟空间并复制子树序列 
		}		
		str01[i] = '\0';
		str02[i] = '\0';
		bt->LChild = NULL;
		//递归调用创建右子树 
		bt->RChild = createBiTreePI(bt->RChild, str01, str02);
		return bt;
	}
	else if(index == strlen(str2)-1)
	{
		//若 index 为 串序列的最后一个字符, 表示无右子树 
		int i, j;
		char str01[strlen(str1)-1] = {'\0'};
		char str02[strlen(str2)-1] = {'\0'};
		for(i = 0, j = 1; i < strlen(str1)-1; i++, j++)
		{
			str01[i] = str1[j];
			str02[i] = str2[i];  			//开辟空间并复制子树序列 
		}
		str01[i] = '\0';
		str02[i] = '\0';
		//递归调用创建左子树 
		bt->LChild = createBiTreePI(bt->LChild, str01, str02);
		bt->RChild = NULL;
		return bt;
	}
	else
	{
		//若 index 在 B 串中间 
		int i, j;
		char str01[index] = {'\0'};
		char str02[index] = {'\0'};
		char str03[strlen(str1)-1-index] = {'\0'};
		char str04[strlen(str1)-1-index] = {'\0'};
		for(i = 0, j = 1; i < index; i++, j++)
		{
			str01[i] = str1[j];
			str02[i] = str2[i];
		}
		str01[i] = '\0';
		str02[i] = '\0';		//复制左子树序列 
		for(i = index+1; i < strlen(str1); i++)
		{
			str03[i-index-1] = str1[i];
			str04[i-index-1] = str2[i];
		}
		str03[i-index-1] = '\0';
		str04[i-index-1] = '\0';		//复制右子树序列
		//递归创建左子树 
		bt->LChild = createBiTreePI(bt->LChild, str01, str02);
		//递归创建右子树 
		bt->RChild = createBiTreePI(bt->RChild, str03, str04);
		return bt;
	}
}



//采用递归算法根据树的中序和后序创建树 
BiTree createBiTreeIP(BiTree bt, char str1[], char str2[]) //根据树的中序和后序创建树 
{
	//采用递归算法 
	int index;
	bt = (Node *)malloc(LEN);
	bt->data = str1[strlen(str1)-1];		//首先读入序列 str1 的最后一个字母, 为树根 
	if(strlen(str1) == 1)	//若串 的长度为 1, 说明其无左右子串 
	{
		bt->LChild = NULL;
		bt->RChild = NULL;
		return bt;
	}
	for(int i = 0; i < strlen(str2); i++)
    {
    	if(str2[i] == str1[strlen(str1)-1])
	    {
	    	index = i;
	    	break;
		}
	}						//在串 B 中查找 串A首字符 所在的位置 index 
	if(index == 0)	
	{
        //若 index 为 0, 表示此树仅有右子树 , 无左子树 
		int i, j;
		char str01[strlen(str1)-1] = {'\0'};
		char str02[strlen(str2)-1] = {'\0'};
		for(i = 0, j = 1; i < strlen(str1)-1; i++, j++)
		{
			str01[i] = str1[i];
			str02[i] = str2[j];			//开辟空间并复制子树序列 
		}		
		str01[i] = '\0';
		str02[i] = '\0';
		bt->LChild = NULL;
		//递归调用创建右子树 
		bt->RChild = createBiTreeIP(bt->RChild, str01, str02);
		return bt;
	}
	else if(index == strlen(str2)-1)
	{
		//若 index 为 串序列的最后一个字符, 表示无右子树 
		int i, j;
		char str01[strlen(str1)-1] = {'\0'};
		char str02[strlen(str2)-1] = {'\0'};
		for(i = 0; i < strlen(str1)-1; i++)
		{
			str01[i] = str1[i];
			str02[i] = str2[i];  			//开辟空间并复制子树序列 
		}
		str01[i] = '\0';
		str02[i] = '\0';
		//递归调用创建左子树 
		bt->LChild = createBiTreeIP(bt->LChild, str01, str02);
		bt->RChild = NULL;
		return bt;
	}
	else
	{
		//若 index 在 B 串中间 
		int i, j;
		char str01[index] = {'\0'};
		char str02[index] = {'\0'};
		char str03[strlen(str1)-1-index] = {'\0'};
		char str04[strlen(str1)-1-index] = {'\0'};
		for(i = 0; i < index; i++)
		{
			str01[i] = str1[i];
			str02[i] = str2[i];
		}
		str01[i] = '\0';
		str02[i] = '\0';		//复制左子树序列 
		for(i = index; i < strlen(str1)-1; i++)
		{
			str03[i-index] = str1[i];
			str04[i-index] = str2[i+1];
		}
		str03[i-index] = '\0';
		str04[i-index] = '\0';		//复制右子树序列
		//递归创建左子树 
		bt->LChild = createBiTreeIP(bt->LChild, str01, str02);
		//递归创建右子树 
		bt->RChild = createBiTreeIP(bt->RChild, str03, str04);
		return bt;
	} 
}



//输出二叉树中的结点 
//先序输出二叉树中的结点 
void prePrintNode(BiTree bt)
{
	if(bt == NULL) return;
	else
	{
		printf("%c\n", bt->data);	//输出根节点 
		prePrintNode(bt->LChild);	//遍历左子树 
		prePrintNode(bt->RChild);	//遍历右子树 
	}
}

//中序输出二叉树中的结点
void inPrintNode(BiTree bt)
{
	if(bt == NULL) return;
	else
	{
		inPrintNode(bt->LChild);	//遍历左子树 
		printf("%c\n", bt->data);	//输出根节点 
		inPrintNode(bt->RChild);	//遍历右子树 
	}
}

//后序输出二叉树中的结点
void postPrintNode(BiTree bt)
{
	if(bt == NULL) return;
	else
	{
		postPrintNode(bt->LChild);	//遍历左子树 
		postPrintNode(bt->RChild);	//遍历右子树 
		printf("%c\n", bt->data);	//输出根节点 
	}
}



//统计二叉树中的结点
//先序统计二叉树中的结点
int preGetCount(BiTree bt)
{
	int count = 0;
	if(bt == NULL) return 0;
	else
	{
		count++;
		count += preGetCount(bt->LChild);
		count += preGetCount(bt->RChild);
	}
	return count;
}

//中序统计二叉树中的结点
int inGetCount(BiTree bt)
{
	int count = 0;
	if(bt == NULL) return 0;
	else
	{
		count += inGetCount(bt->LChild);
		count++;
		count += inGetCount(bt->RChild);
	}
	return count;
}

//后序统计二叉树中的结点
int postGetCount(BiTree bt)
{
	int count = 0;
	if(bt == NULL) return 0;
	else
	{
		count += postGetCount(bt->LChild);
		count += postGetCount(bt->RChild);
		count++;
	}
	return count;
}


//输出二叉树中的叶子结点 
//先序输出二叉树中的叶子结点 
void prePrintLeaf(BiTree bt)
{
	if(bt == NULL) return;
	else
	{
		if(bt->LChild == NULL && bt->RChild == NULL)
		printf("%c\n", bt->data);	//输出根节点 
		prePrintLeaf(bt->LChild);	//遍历左子树 
		prePrintLeaf(bt->RChild);	//遍历右子树 
	}
}

//中序输出二叉树中的叶子结点
void inPrintLeaf(BiTree bt)
{
	if(bt == NULL) return;
	else
	{
		inPrintLeaf(bt->LChild);	//遍历左子树 
		if(bt->LChild == NULL && bt->RChild == NULL)
		printf("%c\n", bt->data);	//输出根节点 
		inPrintLeaf(bt->RChild);	//遍历右子树 
	}
}

//后序输出二叉树中的叶子结点
void postPrintLeaf(BiTree bt)
{
	if(bt == NULL) return;
	else
	{
		postPrintLeaf(bt->LChild);	//遍历左子树 
		postPrintLeaf(bt->RChild);	//遍历右子树 
		if(bt->LChild == NULL && bt->RChild == NULL)
		printf("%c\n", bt->data);	//输出根节点 
	}
}


//统计叶子结点数目
//先序遍历统计二叉树中叶子结点的数目
void leafCount01(BiTree bt)
{
	if(bt == NULL) return;
	else
	{
		if(bt->LChild == NULL && bt->RChild == NULL)
		leafCount++;
		leafCount01(bt->LChild);
		leafCount01(bt->RChild);
	}
}

//中序遍历统计二叉树中叶子结点的数目
void leafCount02(BiTree bt)
{
	if(bt == NULL) return;
	else
	{
		leafCount02(bt->LChild);
		if(bt->LChild == NULL && bt->RChild == NULL)
		leafCount++;
		leafCount02(bt->RChild);
	}
}

//后序遍历统计二叉树中叶子结点的数目
void leafCount03(BiTree bt)
{
	if(bt == NULL) return;
	else
	{
		leafCount03(bt->LChild);
		leafCount03(bt->RChild);
		if(bt->LChild == NULL && bt->RChild == NULL)
		leafCount++;
	}
}

/*采用分治算法, 如果是空树, 返回 0;
如果只有一个结点返回 1;
否则为左右子树叶子结点数之和
*/
int leaf(BiTree bt)
{
	int leaf_count;
	if(bt == NULL)
	leaf_count = 0;
	else if((bt->LChild == NULL) && (bt->RChild == NULL))
	leaf_count = 1;
	else	//叶子数目为左右子树的叶子数目的和 
	leaf_count = leaf(bt->LChild) + leaf(bt->RChild);
	return leaf_count;
}


//后序遍历求二叉树高度的递归算法
int postTreeDepth(BiTree bt)
{
	int hl, hr, max;
	if(bt != NULL)
	{
		hl = postTreeDepth(bt->LChild);		//求左子树深度 
		hr = postTreeDepth(bt->RChild);		//求右子树深度 
		max = hl > hr ? hl : hr;	//获取左右子树深度最大值 
		return max+1;	//返回树的深度 
	}
	else return 0;		//若为空树返回 0 
}

//先序遍历求二叉树高度的递归算法
void preTreeDepth(BiTree bt, int h)
{
	/*先序遍历求二叉树的bt高度的递归算法, h 为 bt 指向结点所在层次
	初值为 1, depth为当前求得的最大层次, 为全局变量,调用前初值为0*/
	if(bt != NULL)
	{
		if(h > depth) depth = h;	//如果该结点层次值大于depth,更新depth的值 
		preTreeDepth(bt->LChild, h+1);		//遍历左子树 
		preTreeDepth(bt->RChild, h+1);		//遍历右子树 
	}
}

//按照树状打印二叉树
void PrintTree(BiTree bt, int nLayer)
{
	if(bt == NULL) return;
	PrintTree(bt->RChild, nLayer+1);
	for(int i = 0; i < nLayer; i++)
	printf("    ");	//按照中序输出结点,用层深决定的左、右位置 
	printf("%c\n", bt->data);
	PrintTree(bt->LChild, nLayer+1);
}




//二叉树的一些基本操作 
