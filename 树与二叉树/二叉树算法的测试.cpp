#include<stdio.h>
#include"二叉树的算法.h"
int main()
{
	BiTree bt;
	char str1[11] = {'\0'};
	char str2[11] = {'\0'};
	puts("请输入此二叉树的先序序列与中序序列(创建二叉树):");
	scanf("%s%s", str1, str2);
	puts("此二叉树逆时针旋转90度后的树状如下:\n"); 
	bt = createBiTreePI(bt, str1, str2);	//先序中序创建二叉树 
	PrintTree(bt, 0);	//打印逆时针旋转90度后的二叉树
	puts("请输入此二叉树的后序序列与中序序列(创建二叉树):");
	scanf("%s%s", str1, str2);
	bt = createBiTreeIP(bt, str1, str2);	//后序中序创建二叉树 
	puts("此二叉树逆时针旋转90度后的树状如下:\n");
	PrintTree(bt, 0);   //打印逆时针旋转90度后的二叉树 
	return 0;
}
//数据测试：
//树：ABCD...EF..G..H.IJ..K.. 
//数据
/* ABCDEFGHIJK
   DCBFEGAHJiK       */ 
   
/*先序创建二叉树样例：先序 ABCDEFGHIJK 中序 DCBFEGAHJIK*/
/*后序创建二叉树样例：后序 DCBFJIHGEA 中序 BCDAFEHJIG*/
