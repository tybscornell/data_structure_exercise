#include<stdio.h>
#include<stdlib.h>
typedef char TypeElem;

typedef struct Node{
	TypeElem data;			//数据 
	Node *LChild, *RChild;	//定义指向左孩子的指针域和指向右孩子的指针域 
	int Ltag, Rtag;			//定义标识符 Ltag 和 Rtag 
}*BiTree,pre;
// Ltag = 0 时, 此结点具有左孩子,LChild指向其左孩子; Ltag = 1 时, 此结点无左孩子, LChild指向其前驱结点
// Rtag = 0 时, 此结点具有右孩子,RChild指向其右孩子; Rtag = 1 时, 此结点无右孩子, RChild指向其后继结点

//先序建立线索二叉树 
void  PreThread(BiTree root)
{
	//对 root所指的二叉树进行先序线索化,其中pre始终指向刚访问过的结点,其初值为 NULL
	if(root != NULL)
	{
	    pre = root;				//当前访问结点为下一个访问结点的前驱 
	    PreThread(root->LChild);
	    if(root->LChild == NULL)
	    {
	    	root->Ltag = 1;			//置前驱线索 
	    	root->LChild = pre;
		}
		if(pre != NULL && pre->RChild == NULL)
		{
			pre->RChild = root;
			pre->Rtag = 1;			//置后置线索 
		}
		PreThread(root->RChild);
    } 
}

//中序建立线索二叉树 
void InThread(BiTree root)
{
	//对root所指的二叉树进行中序线索化,其中pre始终指向刚访问过的结点,其初值为 NULL 
	if(root != NULL)
	{
		Inthread(root->LChild);
		if(root->LChild == NULL)
		{
			root->Ltag = 1;
			root->LChild = pre;			//置前驱线索 
		}
		if(pre != NULL && pre->RChild == NULL)
		{
            pre->RChild = root;
            pre->Rtag = 1;				//置后继线索 
		}
		pre = root;
		Inthread(root->RChild);
	}
}

//后序建立线索二叉树
void PostThread(BiTree root)
{
	//对root所指的二叉树进行后序线索化,其中pre始终指向刚访问过的结点,其初值为 NULL 
    if(root != NULL)
	{
	    PostThread(root->LChild);
		if(root->LChild == NULL)
		{
		    root->Ltag = 1;			//置前驱线索 
			root->LChild = pre;	
		}
		if(pre != NULL && pre->RChild == NULL)
		{
			pre->RChild = root;		//置后驱线索 
			pre->Rtag = 1;
		}
		PostThread(root->RChild);
		pre = root;
	}
}


//在中序树中找结点前驱
BiTree InPre(BiTree p)
{
	//在中序线索二叉树中查找p的中序前驱,并用pre指针返回结果
	BiTree pre = NULL, q = NULL;
	if(p->Ltag == 1) pre = p->LChild;
	else
	{
		//在 p 的左子树中查找"最右下端 "结点
        for(q = p->LChild; q->Rtag == 0; q = q->RChild);
        pre = q;
	}
	return pre;
}


//在中序线索中找结点后继
BiTree InNext(BiTree p)
{
	//在中序线索二叉树中查找 p 的中序后继结点, 并用 next 指针返回结果 
	BiTree next = NULL, q = NULL;
	if(p->Rtag == 1) next = p->RChild;	//直接利用线索 
	else
	{
		//在 p 的右子树中查找 "最左下端 " 结点 
		for(q = p->RChild; q->Ltag == 0; q = q->Ltag);
		next = q;
	}
	return next;
}


//在中序线索树上求中序遍历的第一个结点
BiTree InFirst(BiTree bt)
{
	BiTree p = bt;
	if(!p) return NULL;
	while(p->Ltag == 0) p = p->LChild;
	return p;
}


//遍历中序二叉树
void TInOrder(BiTree bt)
{
    BiTree p;
	p = InFirst(bt);
	while(p)
	{
		printf("%c\n", p->data);
		p = InNext(p);
	}
}



//线索化二叉树的基本操作 
