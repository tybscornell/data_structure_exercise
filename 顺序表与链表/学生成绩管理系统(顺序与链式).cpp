#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#define MAXSIZE 100
#define OK 1
#define ERROR 0
#define TRUE 1
#define FALSE 0
struct student_one{
	int sno;
	char name[10];//sacnf("%s",name);gets(name);
	float grade;//printf("%s",name);puts(name);
};
typedef struct student_two{
	int sno;
	char name[10];//sacnf("%s",name);gets(name);
	float grade;//printf("%s",name);puts(name);
	struct student_two *next;
}Node, *LinkList;

typedef struct{
	struct student_one sdone[MAXSIZE];
	int last;
}SeqList;
typedef student_one ElemType;

//函数的声明 
void theMenu();
void arrayStorageMenu();
void linkedListMenu();
void colorMenu();
int allMySelect(int i, SeqList *L, student_two *sdtwo1);
void setupColor(char color[]);

SeqList *partOneSelect(int i, SeqList *L);
void initList(SeqList *L);
int insList(SeqList *L, int i, ElemType e);
int delList(SeqList *L, int i, ElemType *e);
int modifyData(SeqList *L);
int checkData(SeqList *L);
int listLength(SeqList *L);
void printAll(SeqList *L);
ElemType getData(SeqList *L, int i);
int Locate(SeqList *L, ElemType e);

LinkList partTwoSelect(int i, LinkList L);
LinkList initList02(LinkList L);
void creatFromHead(LinkList L);
void creatFromTail(LinkList L);
Node *Get(LinkList L, int i);
int insList02(LinkList L, int i, Node *e);
int delList02(LinkList L, int i, Node *e);
int modifyList02(LinkList L, int i, Node *e);
Node *Locate02(LinkList L, int sno1);
int listLength02(LinkList L);
void printAll02(LinkList L);




//总菜单 
void theMenu()
{
	printf("\n\n\t- - - - - - - 学生成绩管理系统 - - - - - - -\n\n");
	printf("\t|\t\t\t\t\t   |\n");
	printf("\t|   1. 采用线性表的顺序存储管理学生成绩    |\n");
	printf("\t|\t\t\t\t\t   |\n");
	printf("\t|   2. 采用线性表的链式存储管理学生成绩    |\n");
	printf("\t|\t\t\t\t\t   |\n");
	printf("\t|   3. 颜色设置\t\t\t\t   |\n");
	printf("\t|\t\t\t\t\t   |\n");
	printf("\t|   4. 退出\t\t\t\t   |\n");
	printf("\n\n\t请输入(输入正确的序号):\t");
}

//顺序存储菜单 
void arrayStorageMenu()
{
	printf("\n\n\n\n\t- - - - - - 顺序存储管理学生成绩 - - - - - -\n\n");
	printf("\t|\t\t\t\t\t   |\n");
	printf("\t|   1. 建立新的学生成绩表\t\t   |\n");
	printf("\t|\t\t\t\t\t   |\n");
	printf("\t|   2. 增加新的学生的成绩\t\t   |\n");
	printf("\t|\t\t\t\t\t   |\n");
	printf("\t|   3. 删除表中学生的成绩\t\t   |\n");
	printf("\t|\t\t\t\t\t   |\n");
	printf("\t|   4. 修改表中学生的成绩\t\t   |\n");
	printf("\t|\t\t\t\t\t   |\n");
	printf("\t|   5. 查找表中学生的成绩\t\t   |\n");
	printf("\t|\t\t\t\t\t   |\n");
	printf("\t|   6. 查找学生信息的位置\t\t   |\n");
	printf("\t|\t\t\t\t\t   |\n");
	printf("\t|   7. 随机打印线性表中某一位置的信息\t   |\n");
	printf("\t|\t\t\t\t\t   |\n");
	printf("\t|   8. 返回表中学生总人数\t\t   |\n");
	printf("\t|\t\t\t\t\t   |\n");
	printf("\t|   9. 输出打印学生成绩表\t\t   |\n");
	printf("\t|\t\t\t\t\t   |\n");
	printf("\t|   10. 退出并删除此成绩表\t\t   |\n");
	printf("\n\n\t请输入(输入正确的序号):\t");
}

//链式存储菜单 
void linkedListMenu()
{
	printf("\n\n\n\n\t- - - - - - 链式存储管理学生成绩 - - - - - -\n\n");
	printf("\t|\t\t\t\t\t   |\n");
	printf("\t|   1. 初始化学生成绩表    \t\t   |\n");
	printf("\t|\t\t\t\t\t   |\n");
	printf("\t|   2. 建立学生成绩表    \t\t   |\n");
	printf("\t|\t\t\t\t\t   |\n");
	printf("\t|   3. 增加学生成绩\t\t\t   |\n");
	printf("\t|\t\t\t\t\t   |\n");
	printf("\t|   4. 删除学生成绩\t\t\t   |\n");
	printf("\t|\t\t\t\t\t   |\n");
	printf("\t|   5. 修改学生成绩\t\t\t   |\n");
	printf("\t|\t\t\t\t\t   |\n");
	printf("\t|   6. 查找学生成绩\t\t\t   |\n");
	printf("\t|\t\t\t\t\t   |\n");
	printf("\t|   7. 返回学生人数\t\t\t   |\n");
	printf("\t|\t\t\t\t\t   |\n");
	printf("\t|   8. 打印学生成绩表    \t\t   |\n");
	printf("\t|\t\t\t\t\t   |\n");
	printf("\t|   9. 退出并删除此成绩表\t\t   |\n");
	printf("\n\n\t请输入(输入正确的序号):\t");
}

//颜色菜单 
void colorMenu()
{
	printf("\n\n\t\t----------颜-色-菜-单----------\n\n");
	printf("\t\t0.黑色\t\t\t8.灰色\n\n");
	printf("\t\t1.蓝色\t\t\t9.淡蓝色\n\n");
	printf("\t\t2.绿色\t\t\tA.淡绿色\n\n");
	printf("\t\t3.湖蓝色\t\tB.淡浅绿色\n\n");
	printf("\t\t4.红色\t\t\tC.淡红色\n\n");
	printf("\t\t5.紫色\t\t\tD.淡紫色\n\n");
	printf("\t\t6.黄色\t\t\tE.淡黄色\n\n");
	printf("\t\t7.白色\t\t\tF.亮白色\n\n");
}

//总菜单选择器 
int allMySelect(int i, SeqList *L, LinkList Lt)
{
	int n;
	switch(i)
	{
		case 1:
			//顺序表操作 
			do{
				arrayStorageMenu();
				scanf("%d", &n);
				if (n > 10 && n < 1)
				{
					printf("\n\t输入有误,请再次输入!\n");
					continue;
				}
				else if (n == 10) break;
				else partOneSelect(n,L);
			}while(TRUE);
			break;
		case 2:
			//链式表操作 
			do{
				linkedListMenu();
				scanf("%d", &n);
				if (n > 9 && n < 1)
				{
					printf("\n\t输入有误,请再次输入!\n");
					continue;
				}
				else if (n == 9) break;
				else Lt = partTwoSelect(n, Lt);
			}while(TRUE);
			break;
		case 3:
			//颜色设置 
			do{
				char color[9] = {"color "};
				setupColor(color);
				system(color);
				printf("\n更改成功!\n请选择：\n\t1.继续修改\n\t2.确定\n");
				scanf("%d", &n);
			}while(n == 1);
			break;
	}
}

//颜色设置器,返回一组字符串进行颜色设置 
void setupColor(char color[])
{
	char c = '\0';
	colorMenu();
	getchar();
	printf("\n\n请输入背景颜色序号：\t");
	do{
		c=getchar();
		if(c == ' ' || c == '\n') printf("\n\n输入有误,请重新输入!\n");
		else break;
	}while(TRUE);
	color[6] = c;
	printf("\n设置背景颜色成功!");
	getchar();
	printf("\n\n请输入字体颜色序号：\t");
	do{
		c=getchar();
		if(c == ' ' || c == '\n') printf("\n\n输入有误,请重新输入!\n");
		else break;
	}while(TRUE);
	color[7] = c;
	color[8] = '\0';
}


//顺序存储菜单选择器 
SeqList *partOneSelect(int i, SeqList *L)
{
	int n, j;
	ElemType e, *e1 = &e;
	switch(i)
	{
		case 1:
			//将线性表初始化为空表 
			do{
				initList(L);
				printf("\n\n请选择：\t1.进行操作\n请输入：\t");
				scanf("%d", &n);
			}while(n != 1);
			break;
		case 2:
			//为线性表插入新的元素 
			do{
				printf("请输入插入的学生信息(学号、姓名、成绩)：\n");
				scanf("%d%s%f",&e1->sno, e1->name, &e1->grade);
				printf("请输入插入线性表中的位置(线性表已有 %d 数据)：\n", L->last + 1);
				scanf("%d", &j);
				insList(L, j, e);
				printf("\n\n请选择：\t1.继续插入\t2.返回\n请输入：\t");
				scanf("%d", &n);
			}while(n != 2);
			break;
		case 3:
			//删除线性表中的某一元素 
			do{
				printf("请输入删除线性表中学生的位置(线性表已有 %d 数据)：\n", L->last + 1);
				scanf("%d", &j);
				delList(L, j, e1);
				printf("\n\n请选择：\t1.继续删除\t2.返回\n请输入：\t");
				scanf("%d", &n);
			}while(n != 2);
			break;
		case 4:
			//修改线性表中某一元素的数据 
			do{
				modifyData(L);
				printf("\n\n请选择：\t1.继续修改\t2.返回\n请输入：\t");
				scanf("%d", &n);
			}while(n != 2);
			break;
		case 5:
			//根据学号查询查询线性表中的数据 
			do{
				checkData(L);
				printf("\n\n请选择：\t1.继续查找\t2.返回\n请输入：\t");
				scanf("%d", &n);
			}while(n != 2);
			break;
		case 6:
			//查找学生在线性表(学生成绩信息表)中的位置 
			do{
				printf("请输入查找的学生信息(学号、姓名、成绩)：\n");
				scanf("%d%s%f",&e1->sno, e1->name, &e1->grade);
				printf("该学生在线性表(学生成绩信息表)中的第 %d 个位置\n", Locate(L, e));
				printf("\n\n请选择：\t1.继续查找\t2.返回\n请输入：\t");
				scanf("%d", &n);
			}while(n != 2);
			break;
		case 7:
			//通过线性表序号打印该学生的信息 
			do{
				printf("请输入获取线性表中的元素的序号：\n");
				scanf("%d", &j); 
				e = getData(L, j);
				printf("查找成功!\n此学生成绩信息为：\n");
				printf("学号：\t%d\n姓名：\t%s\n成绩：\t%.2f\n", e.sno, e.name, e.grade);
				printf("\n\n请选择：\t1.继续获取打印\t2.返回\n请输入：\t");
				scanf("%d", &n);
			}while(n != 2);
			break;
		case 8:
			//返回线性表中的所有记录 
			do{
				printf("线性表(学生成绩信息表)已有 %d 人\n",listLength(L));
				printf("\n\n请选择：\t1.返回\t2.刷新\n请输入：\t");
				scanf("%d", &n);
			}while(n != 1);
			break;
		case 9:
			//打印输出线性表的所有数据 
			do{
				printAll(L);
				printf("\n\n请选择：\t1.返回\t2.刷新\n请输入：\t");
				scanf("%d", &n);
			}while(n != 1);
			break;
	}
	return L;
}

//顺序存储操作函数
// 初始化顺序表(学生成绩信息表) 
void initList(SeqList *L)
{
	L->last = -1;
	printf("新的线性表(学生成绩信息表)已经成功建立!\n");
}

//添加顺序表中的学生成绩信息数据(学生成绩信息表) 
int insList(SeqList *L, int i, ElemType e)
{
	if(i < 1 || i > L->last+2)
	{
		printf("插入位置 %d 不合法!\n", i);
		return ERROR;		
	}
	if(L->last >= MAXSIZE - 1)
	{
		printf("线性表(学生成绩信息表)已存满数据,无法插入!\n");
		return ERROR;
	}
	for(int k = L->last; k >= i-1; k--)
	{
		L->sdone[k+1] = L->sdone[k];
	}
	L->sdone[i-1] = e;
	L->last++;
	printf("线性表(学生成绩管理表)插入数据成功!\n");
	return OK;
}

//删除顺序表(学生成绩信息表)中的某一元素数据 
int delList(SeqList *L, int i, ElemType *e)
{
	if(i < 1 || i > L->last+1)
	{
		printf("删除位置 %d 不合法!\n", i);
		return ERROR;
	}
	if(L->last <= 0)
	{
		printf("线性表(学生成绩信息表)暂无数据,无法删除!\n");
		return ERROR;
	}
	*e = L->sdone[i];
	for(int k = i-1; k < L->last; k++)
	{
		L->sdone[k] = L->sdone[k+1];
	}
	L->last--;
	printf("线性表(学生成绩管理表)删除数据成功!\n");
	return OK;
}

//返回顺序线性表(学生成绩信息表)的长度 
int listLength(SeqList *L)
{
	return L->last + 1;
}

//提取线性表中的单个数据 
ElemType getData(SeqList *L, int i) 
{
	printf("提取完成!\n");
	return L->sdone[i-1];
}

//根据学生信息返回在表中的位置 
int Locate(SeqList *L, ElemType e)
{
	int i = 0;
	while(i < L->last)
	{
		if(L->sdone[i].sno == e.sno && strcmp(L->sdone[i].name, e.name) == 0 && L->sdone[i].grade == e.grade) return i+1;
		i++;
	}
	return -1;
}

//修改表中学生的成绩信息 
int modifyData(SeqList *L)
{
	int n, i = 0;
	float grade01;
	printf("请输入需要修改学生成绩信息的学生学号：\n");
	scanf("%d", &n);
	while(L->sdone[i].sno != n)
	{
		i++;
		if(i > L->last)
		{
			printf("输入有误,学生成绩表中暂无此学生!\n");
			return ERROR;
		}
	}
	printf("请输入此学生新的成绩：\n");
	scanf("%f",&grade01);
	L->sdone[i].grade = grade01;
	printf("修改成绩成功!\n");
	return OK;
}

//查找学生成绩信息 
int checkData(SeqList *L)
{
	int n, i = 0;
	printf("请输入需要查找学生成绩信息的学生学号：\n");
	scanf("%d", &n);
	while(L->sdone[i].sno != n)
	{
		i++;
		if(i > L->last)
		{
			printf("输入有误,学生成绩表中暂无此学生!\n");
			return ERROR;
		}
	}
	printf("查找成功!\n此学生成绩信息为：\n");
	printf("学号：\t%d\n姓名：\t%s\n成绩：\t%.2f\n", L->sdone[i].sno, L->sdone[i].name, L->sdone[i].grade);
}

//打印顺序表(学生成绩信息表)中的所有信息 
void printAll(SeqList *L)
{
	int i = 0;
	while(i < L->last+1)
	{
		if(i == 0) printf("序号\t\t\t学号\t\t\t姓名\t\t\t成绩\t\t\t\t\n\n");
		printf("%d\t\t\t%d \t\t\t%s \t\t\t%.2f \t\t\t\t\n\n", i + 1, L->sdone[i].sno, L->sdone[i].name, L->sdone[i].grade);
		i++;
	}
}

//链式存储菜单选择器 
LinkList partTwoSelect(int i, LinkList L)
{
	int n, j;
	Node e, *e1 = &e;
	switch(i)
	{
		case 1:
			//初始化链式表(学生成绩信息表)
			do{
				L = initList02(L);
				printf("\n\n请选择：\t1.建立成功,继续操作\n请输入：\t");
				scanf("%d", &n);
			}while(n != 1);
			break;
		case 2:
			//建立链式表(学生成绩信息表)
			do{
				printf("\n请选择：\n1.采用头插法建立链式表\t\t2.采用尾插法建立链式表\n请输入：\t");
				scanf("%d", &j);
				if(j == 1) creatFromHead(L);
				else creatFromTail(L);
				printf("建立链式表格成功!\n\n\t请选择：\t1.继续插入累加\t2.返回\n请输入：\t");
				scanf("%d", &n);
			}while(n == 1);
			break;
		case 3:
			//增加链式表中的学生数据(学生成绩信息表)
			do{
				printf("请输入此学生的信息(学号、姓名、成绩)：\n");
				scanf("%d%s%f", &e1->sno, e1->name, &e1->grade);
				printf("请输入插入的位置(目前表中有 %d 个学生成绩数据)：\t", L->sno);
				scanf("%d", &j);
				if(insList02(L, j, e1)) printf("插入成功!\n\n\t请选择：\t1.继续插入\t2.返回\n请输入：\t");
				else printf("\n\n\t请选择：\t1.继续插入\t2.返回\n请输入：\t");
				scanf("%d", &n);
			}while(n == 1);
			break;
		case 4:
			//删除链式表中的学生数据(学生成绩信息表)
			do{
				printf("请输入删除学生成绩信息的表中位置(目前表中有 %d 个学生成绩数据)：\t", L->sno);
				scanf("%d", &j);
				if(delList02(L, j, e1)) printf("删除成功!\n\n\t请选择：\t1.继续删除\t2.返回\n请输入：\t");
				else printf("\n\n\t请选择：\t1.继续删除\t2.返回\n请输入：\t");
				scanf("%d", &n);
			}while(n == 1);
			break;
		case 5:
			//修改链式表中的学生数据(学生成绩信息表) 
			do{
				printf("请输入修改学生成绩信息的表中位置(目前表中有 %d 个学生成绩数据)：\t", L->sno);
				scanf("%d", &j);
				if(modifyList02(L, j, e1)) printf("修改成功!\n\n\t请选择：\t1.继续修改\t2.返回\n请输入：\t");
				else printf("\n\n\t请选择：\t1.继续修改\t2.返回\n请输入：\t");
				scanf("%d", &n);
			}while(n == 1);
			break;
		case 6:
			//查找链式表中的学生数据(学生成绩信息表),分为按值查找和序号查找 
			do{
				int m, sno1;
				printf("请选择：\n1.按照信息的值进行查找\t2.按照表中序号进行查找\n\n请输入：\t");
				scanf("%d", &m);
				if(m == 1)
				{
					printf("\n\n请输入需要查找学生成绩的学号：\t");
					scanf("%d", &sno1);
					e1 = Locate02(L,sno1);
					if(e1 != NULL)
					{
						printf("该学生成绩信息如下：\n\n学号：%d\t\t姓名：%s\t\t成绩：%.2f\n", e1->sno, e1->name, e1->grade);
					}
				}
				else
				{
					printf("请输入查找学生成绩信息的表中位置(目前表中有 %d 个学生成绩数据)：\t", L->sno);
					scanf("%d", &j);
					e1 = Get(L, j);
					if(e1 != NULL)
					{
						printf("该学生成绩信息如下：\n\n学号：%d\t\t姓名：%s\t\t成绩：%.2f\n", e1->sno, e1->name, e1->grade);
					}
				}
				printf("\n\t请选择：\t1.继续查找\t2.返回\n请输入：\t");
				scanf("%d", &n);
			}while(n == 1);
			break;
		case 7:
			//返回链式表中总的人数 
			do{
				printf("学生成绩信息表中共有 %d 人", listLength02(L));
				printf("\n\t请选择：\t1.刷新\t2.返回\n请输入：\t");
				scanf("%d", &n);
			}while(n == 1);
			break;
		case 8:
			//打印所有学生成绩信息 
			do{
				printAll02(L);
				printf("\n\t请选择：\t1.刷新\t2.返回\n请输入：\t");
				scanf("%d", &n);
			}while(n == 1);
			break;
	}
	return L;
}

//初始化链表 
LinkList initList02(LinkList L)
{
	L = (Node *)malloc(sizeof(Node));
	L->next = NULL;
	L->sno = 0;
	printf("初始化链式表(学生成绩信息表)成功!\n");
	return L;
}

//头插法建立链表 
void creatFromHead(LinkList L)
{
	Node *s;
	int n;
	printf("请输入初次建立数据表数据总数(学生成绩信息总人数)：\n");
	scanf("%d" ,&n);
	for(int i = 1; i <= n; i++)
	{
		s = (Node *)malloc(sizeof(Node));
		printf("请输入第 %d 名同学的学号、姓名、成绩信息：\n", i);
		scanf("%d%s%f", &s->sno, s->name, &s->grade);
		s->next = L->next;
		L->next = s;
		L->sno++;
	}
}

//尾插法建立链表 
void creatFromTail(LinkList L)
{
	Node *r = L, *s;
	int n;
	printf("请输入初次建立数据表数据总数(学生成绩信息总人数)：\n");
	scanf("%d" ,&n);
	while(r->next != NULL) r = r->next; 
	for(int i = 1; i <= n; i++)
	{
		s = (Node *)malloc(sizeof(Node));
		printf("请输入第 %d 名同学的学号、姓名、成绩信息：\n", i);
		scanf("%d%s%f", &s->sno, s->name, &s->grade);
		r->next = s;
		r = s;
		L->sno++;
	}
	r->next = NULL;
}

//按照序号取得某一结点 
Node *Get(LinkList L, int i)
{
	Node *p = L;
	if(L->sno < 1)
	{
		printf("链式线性表(学生成绩信息表)暂无数据!\n");
		return NULL;
	}
	if(i < 1 || i > L->sno)
	{
		printf("查找 %d 个结点有误(目前暂有 %d 个结点)!\n",i,L->sno);
		return NULL;
	}
	for(int j = 1; j <= i; j++)
	{
		p = p->next;
	}
	return p;
}

//根据学号定位返回结点 
Node *Locate02(LinkList L, int sno1)
{
	Node *p = L->next;
	if(L->sno < 1)
	{
		printf("链式线性表(学生成绩信息表)暂无数据!\n");
		return NULL;
	}
	while(p->sno != sno1 && p != NULL)
	p = p->next;
	if(p == NULL)
	{
		printf("未找到与此值相对应的结点!\n");
		return NULL;
	}
	return p;
}

//插入学生信息 
int insList02(LinkList L, int i, Node *e)
{
	Node *pre = L, *s = (Node *)malloc(sizeof(Node));
	int k = 0;
	if(i <= 0) return ERROR;
	while(pre != NULL && k < i-1)
	{
		pre = pre->next;
		k++;
	}
	if(pre == NULL)
	{
		printf("插入位置不合理!\n失败!\n");
		return ERROR;
	}
	//复制结点内容 
	s->sno = e->sno;
	strcpy(s->name, e->name);
	s->grade = e->grade;
	
	s->next = pre->next;
	pre->next = s;
	L->sno++;
	return OK;
}

//删除学生信息 
int delList02(LinkList L, int i, Node *e)
{
	Node *pre = L, *r;
	int k = 0;
	if(i < 1 || i > L->sno)
	{
		printf("删除位置不合理!\n失败!\n");
		return ERROR;
	}
	while(pre->next != NULL && k < i-1)
	{
		pre = pre->next;
		k++;
	}
	if(pre->next == NULL)
	{
		printf("删除位置不合理!\n失败!\n");
		return ERROR;
	}
	r = pre->next;
	pre->next = r->next;
	e->sno = r->sno;
	strcpy(e->name, r->name);
	e->grade = r->grade;
	free(r);
	L->sno--;
	return OK;
}

//修改学生信息 
int modifyList02(LinkList L, int i, Node *e)
{
	Node *r = L;
	int k = 0;
	if(i < 1 || i > L->sno)
	{
		printf("位置输入有误,暂无该同学信息,修改失败!\n");
		return ERROR;
	}
	while(k < i)
	{
		r = r->next;
		k++;
	}
	e->sno = r->sno;
	strcpy(e->name, r->name);
	e->grade = r->grade;
	printf("原学生成绩信息如下：\n\n学号：%d\t\t姓名：%s\t\t成绩：%.2f\n", r->sno, r->name, r->grade);
	printf("\n请输入新的信息(学号、姓名、成绩):\n");
	scanf("%d%s%f",&r->sno, r->name, &r->grade);
	return OK;
}

//返回链表结点的个数 
int listLength02(LinkList L)
{
	return L->sno;
}

//打印链式线性表(学生成绩信息表) 
void printAll02(LinkList L)
{
	int i = 0;
	Node *r = L->next;
	if(r == NULL)
	{
		printf("成绩表中暂无学生成绩信息!\n");
	}
	printf("序号\t\t\t学号\t\t\t姓名\t\t\t成绩\t\t\t\t\n\n");
	while(r != NULL)
	{
		printf("%d\t\t\t%d \t\t\t%s \t\t\t%.2f \t\t\t\t\n\n", i + 1, r->sno, r->name, r->grade);
		r = r->next;
		i++;
	}
}


//主函数,程序开始的地方 
int main()
{
	system("color E4");
	SeqList L,*L1 = &L;			//定义顺序线性表 
	LinkList L2;				//定义链式线性表 
	int n;
	do{
		theMenu();
		scanf("%d",&n);
		allMySelect(n,L1,L2);
	}while(n != 4);
	return 0;
}


