#include<stdio.h>
#include<stdlib.h>
#define LEN sizeof(Node)
#define MAXSIZE 100
#define OK 1
#define ERROR 0
#define TRUE 1
#define FALSE 0

typedef struct Node{		//线性表顺序表实现串的存储 
	char c[MAXSIZE];
	int len;
}*String;


int strAssign(String S, char *chars);		//初始化串操作 
int strInsert(String S, int pos, String P);			//串的插入操作 
int strDelete(String S, int pos, int len);		// 串的删除操作 
int strCompare(String S, String P);		//串的比较操作 
int strCopy(String S, String P);	//串的复制操作 
int strEmply(String S);			//判断空串的操作 
int strLength(String S);		//求串的长度操作 
int strClear(String S);			//清空串的内容 
int strCat(String S, String P);		//串的连接操作 
int strIndex(String S, int pos, String P);		//索引字串在主串的位置操作 
int subString(String Sub, String S, int pos, int len);		//获取字串的操作 
int strReplace(String S, String T, String V);		//字串的替换操作 
int strDestroy(String S);		//销毁串 
void printStr(String S);		//打印串 



//初始化串操作,生成一个值等于 chars 的串 S 
int strAssign(String S, char *chars)
{
	int i = 0;
	S->len = 0;
	char *c = chars;
	while(*c != '\0' && S->len < MAXSIZE)
	{
		S->c[i] = *c;
		S->len++;
		i++;
		c++;
	}
	return OK;
}

//顺序串的插入函数, 将字符串 t 插入下标为 pos 前的 S 串中 
int strInsert(String S, int pos, String P)
{
	if(pos < 0 || pos > S->len) return ERROR;
	if(S->len + P->len <= MAXSIZE)		//此条件成立说明插入后的串不会超出串最大长度 
	{
		for(int i = S->len + P->len - 1; i >= P->len + pos; i--)
		{
			S->c[i] = S->c[i-P->len];
		}		//pos 后的串后移, 为插入的串空出位置 
		for(int i = 0; i < P->len; i++)
		{
			S->c[i+pos] = P->c[i];
		}		//新串进行插入 
		S->len = S->len + P->len;	//长度变化 
	}
	else if(pos + P->len <= MAXSIZE)
	{						//此条件成立说明插入后的串会超出最大长度, 只能舍弃 pos 后挪的某些部分 
		for(int i = MAXSIZE - 1; i >= P->len + pos - 1; i--)
		{		//pos 后的串后移, 为插入的串空出位置 , 过程中可能丢失部分数据 
			S->c[i] = S->c[i-P->len];
		}
		for(int i = 0; i < P->len; i++)
		{		
			S->c[i+pos] = P->c[i];		//新的串进行插入 
		}
		S->len = MAXSIZE;	//串的长度改变为最大长度 
	}
	else		//此条件说明从 pos 位置插入的新串超出串的总长度 
	{
		for(int i = 0; i < MAXSIZE - pos; i++)
		{
			S->c[i+pos] = P->c[i];		//新的串进行插入 
		}
		S->len = MAXSIZE;	//串的长度变为最大长度 
	}
	return OK;
}

//在串 s 中删除从下标 pos 起的 len 个字符 
int strDelete(String S, int pos, int len)
{
	if(pos < 0 || pos > (S->len - len)) return ERROR;	//pos 或 len 不合理都会导致删除失败 
	for(int i = pos + len; i < S->len; i++)
	{
		S->c[i-len] = S->c[i]; 			//进行删除 
	}
	S->len = S->len - len;		//串的长度改变 
	return OK;
}

//字符串比较函数, S > P 返回 >0 , S = P, 返回 =0, S < P, 返回 <0 
int strCompare(String S, String P)
{
	for(int i = 0; i < S->len && i < P->len; i++)
	{		//两个串进行比较 
		if(S->c[i] != P->c[i]) return S->c[i] - P->c[i];
	}
	return S->len - P->len;		
}

//字符串拷贝, 将 P 字符串的内容拷贝至 S 串 
int strCopy(String S, String P)
{ 
	S->len = P->len;
	for(int i = 0; i < S->len; i++) S->c[i] = P->c[i];
	return OK;	
}

//判断串是否为空 
int strEmply(String S)
{
	if(S->len <= 0) return TRUE;
	else return FALSE;
}

//获取串的长度 
int strLength(String S)
{
	return S->len;
}

//将串 S 置为空串 
int strClear(String S)
{
	for(int i = 0; i < S->len; i++)
	S->c[i] = '\0';
	S->len = 0;
	return OK;
}

//串的连接, 将串 P 连接至 串 S 
int strCat(String S, String P)
{
	if(S->len + P->len > MAXSIZE)	 
	{			//	若连接后的串超出字符串的最大长度
		for(int i = 0; i < MAXSIZE - S->len; i++)
		{
			S->c[i+S->len] = P->c[i];
		}
		S->len = MAXSIZE;
	}
	else
	{		//	若连接后的串未超出字符串的最大长度
		for(int i = 0; i < P->len; i++)
		{
			S->c[i+S->len] = P->c[i];
		}
		S->len = S->len + P->len;
	}
	return OK;
}

void printStr(String S)		//输出串 S 
{
	for(int i = 0; i < S->len; i++)
	printf("%c", S->c[i]);
	printf("\n");
}


//串的简单模式匹配 Brute-Force(布鲁特-福斯)算法 
//最坏时间复杂度较高, 为 O(S->len * P->len) 
int strIndex(String S, int pos, String P)
{
	int start = pos, i = start, j = 0;
	if(P->len == 0) return ERROR;	//若串 P 的长度为空串, 则无法匹配 
	while(i < S->len && j < P->len)
	{
		if(S->c[i] == P->c[j])	//若匹配相等, i 和 j 累加 
		{
			i++;
			j++;
		}
		else	//否则从主串的下一个位置继续开始匹配 
		{
			start++;
			i = start;
			j = 0;
		}
	}
	if(j >= P->len) return start;
	else return -1;
}

//用 Sub 返回串 S 的第 pos 个字符起长度为 len 的字串 
int subString(String Sub, String S, int pos, int len)
{
	if(pos < 0 || pos >= S->len) return ERROR;
	if(pos + len > MAXSIZE)
	{
		len = MAXSIZE - pos;	//若长度超限的话,仅能提取存在范围的串 
	}
	Sub->len = len;
	for(int i = pos; i < pos + len; i++)
	{
		Sub->c[i-pos] = S->c[i];
	}		//复制串 
	return OK;
}

//串 S、T 和 V 存在且 T 是非空串 ,用 串 V 代替 S 的字串 T 
int strReplace(String S, String T, String V)
{
	if(T->len == 0) return ERROR;
	int start = 0, i = start, j = 0;
	while(start < S->len)
	{
		if(S->c[i] == T->c[j])
		{
			i++;
			j++;
		}
		else
		{
			start++;
			i = start;
			j = 0;
		}			//进行 S 串的查找,找到 T 执行以下 
		if(j >= T->len)
		{
			strDelete(S, start, T->len);	//删除字符串 T 
			strInsert(S, start, V);		//插入字符串 V 
			start = start + V->len;		//查询光标后挪 
			j = 0;
		}
	}
	return OK;
}

//销毁串 S; 
int strDestroy(String S)
{
	free(S);
	return OK;
}
