#include<stdio.h>
#include<stdlib.h>
#define LEN sizeof(char)
#define OK 1
#define ERROR 0
#define TRUE 1
#define FALSE 0

/*堆串在程序运行过程中临时分配存储空间,对串的插入 , 替换等操作无长度限制, 
与定长顺序串和链串相比, 这种存储方式是非常有效的和方便的, 但在程序执行过程中
会不断地生成新串和销毁旧串。*/

typedef struct Node{
	char *c;
	int len;
}*String;


//堆串的操作函数 
int initString(String S);		//初始化堆串 操作 
int strAssign(String S, char *chars);		//堆串的初始化赋值操作	
int strInsert(String S, int pos, String P);			//堆串的插入操作 
int strDelete(String S, int pos, int len);		//堆串的删除操作 
int strCopy(String S, String P);		//堆串的拷贝操作 
int strEmpty(String S);					//判断串空操作 
int strCompare(String S, String P);		//串的比较 
int strLength(String S);			//求串的长度 
int strClear(String S);				//清空串 
int strCat(String S, String P);			//将串 P 连接至串 S 操作 
int subString(String Sub, String S, int pos, int len);		//取 主串下标 pos开始的长度为 len 的字串 给 串Sub 
int strIndex(String S, int pos, String P);		//字串在主串的位置检索函数 
int strReplace(String S, String T, String V);		//主串内字串的替换操作 
int strDestroy(String S);	//串的销毁 
void printStr(String S);	//串的打印 


 
//堆串的初始化操作 
int initString(String S)
{
	S->c = '\0';
	S->len = 0;
	return OK;
}

//堆串的初始化赋值函数 
int strAssign(String S, char *chars)
{
	int i = 0;
	char *c1, *c2 = chars, *old = S->c;
	while(*c2 != '\0')
	{
		i++;
		c2++;
	}			//测试字符串长度 
	if(i == 0)
	{
		S->c = '\0';
		S->len = 0;
		return OK;
	}			//长度若为 0 ,直接初始化
	S->len = i;  
	S->c = (char *)malloc(LEN * S->len);
	for(i= 0, c1 = S->c, c2 = chars; i < S->len; i++, c1++, c2++)
	*c1 = *c2;		//将串 chars 复制给 S 
	free(old);
	old = NULL;
	return OK;
}

//将串 P 插入 S 串的 pos(下标) 个位置前 
int strInsert(String S, int pos, String P)
{
	if(pos < 0 || pos > S->len) return ERROR;
	int i = 0;
	char *c1, *c2 = P->c, *temp = S->c, *old = S->c;
	S->len += P->len;
	S->c = (char *)malloc(LEN * S->len);
	
	for(i = 0, c1 = S->c; i < pos; i++, c1++, temp++) *c1 = *temp; 	//c1 为串 S 为 c 新开辟的空间,temp 为 旧的串 S->c的空间 
	for(; i < pos + P->len; i++, c1++, c2++) *c1 = *c2;		//c2 为 P->c的存储空间 
	for(; i < S->len + P->len - 1; i++, c1++, temp++) *c1 = *temp;		//将 pos 位置后的 旧的 S->c 位置再插入到新的 S->c中 去 
	//通过三个循环将旧的 S->c 和 P->c 按照 pos 插入到新的 S->c 中
	free(old);
	old = NULL;
	return OK;
}

//删除串 S 中下标从 pos 开始,长度为 len 的串 
int strDelete(String S, int pos, int len)
{
	if(pos < 0 || pos >= S->len || len > S->len - pos) return ERROR;
	int i = 0;
	char *c1, *c2 = S->c, *old = S->c;
	S->len -= len;		//新串的长度 
	S->c = (char *)malloc(S->len * LEN);	//新串的大小 
	for(i = 0, c1 = S->c; i < pos; i++, c1++, c2++) *c1 = *c2;	//下标 pos 前的复制 
	for(; i <= pos + len -1; i++, c2++);	//将中间部分省略不计 
	for(; i < S->len + len; i++, c1++, c2++) *c1 = *c2;	//pos + len 后串的复制 
	free(old);		//释放原串的空间
	old = NULL; 
	return OK;	
}

//将串 P 复制给串 S 
int strCopy(String S, String P)
{
	if(P->len <= 0) return ERROR;	//若串 P 的长度小于等于 0 ,返回错误 
	int i = 0;
	char *c1, *c2 = P->c, *old = S->c;
	S->len = P->len;	//新串 S 长度 变化 
	S->c = (char *)malloc(S->len * LEN);	//空间大小 变化 
	for(i = 0, c1 = S->c; i < P->len; i++, c1++, c2++)	*c1  = *c2;		//循环复制 
	free(old);
	old = NULL;
	return OK; 
}

//判断是否为空串 S 
int strEmpty(String S)
{
	if(S->len <= 0) return TRUE;
	else return FALSE; 
}

//串 S 和串 P 进行比较 
int strCompare(String S, String P)
{
	int i = 0;
	char *c1 = S->c, *c2 = P->c;
	while(i < S->len && i < P->len)
	{
		if(*c1++ != *c2++) return *c1 - *c2;	//若存在字符不相等,返回差值 
		i++;		
	}        // S > P 返回 > 0, S = P 返回 = 0, S < P 返回 < 0 
	return S->len - P->len;
}

//串 S 的长度 
int strLength(String S)
{
	return S->len;
}

//清空串 S 
int strClear(String S)
{
	char *c = S->c;
	S->len = 0;
	S->c = '\0';	//将 串 S 清空 
	free(c);
	c = NULL;
	return OK;
}

//将串 P 连接至 串 S 后 
int strCat(String S, String P)
{
	int i = 0, oldlen = S->len;		//记录下旧串 S 的长度 
	char *c1, *c2 = P->c, *c3 = S->c, *old = S->c;  //记录它们的空间 
	S->len += P->len;		//确定新串的长度 
	S->c = (char *)malloc(S->len * LEN);	//确定新串 的大小 
	for(c1 = S->c; i < oldlen; i++, c1++, c3++) *c1 = *c3;	//进行旧串复制到新串 
	for(; i < S->len; i++, c1++, c2++) *c1 = *c2;	//将串 P 继续复制到 新串 
	free(old);
	old = NULL;
	return OK;
}

//用 Sub 返回串 S 的第 pos 个字符起长度为 len 的字串 
int subString(String Sub, String S, int pos, int len)
{				//判断条件 
	if(pos < 0 || pos >= S->len || len <= 0 || len > S->len - pos) return ERROR;
	int i = 0;
	char *c1, *c2 = S->c, *old = Sub->c;	//分别记录 串 的空间地址 
	Sub->len = len;		//创建 新串的长度 
	Sub->c = (char *)malloc(Sub->len * LEN);  		//新串的大小 
	for(i = 0; i < pos; i++, c2++);			
	for(c1 = Sub->c; i <= pos + len -1; i++, c1++, c2++) *c1 = *c2;		//进行串的截取 
	free(old);
	old = NULL;
	return OK;
}

//若串 S 中存在和串 T 相同的字串, 请返回 T 在 S中第一次出现的位置 
int strIndex(String S, int pos, String P)
{
	if(pos >= S->len || P->len > S->len - pos) return -1;		//返回下标为 -1 
	int start = pos;	
	char *c1, *c2;
	while(start < S->len)
	{
		int k = 0;
		c1 = S->c + start;
		c2 = P->c;
		if(S->len - start < P->len) return -1;
		while(*c1 == *c2 && k < P->len)		//进行串的 相等匹配 
		{
			k++;
			c1++;
			c2++;
		}
		if(k >= P->len) return start;	//若 K >= P->len 说明匹配成功, 返回字串在主串第一次出现的下标 
		else start++;
	}
	return -1;
}


//用串 V 替换串 S 中出现的所有与 T 相等的不重叠的字符串 
int strReplace(String S, String T, String V)
{
	if(T->len <= 0) return ERROR;
	int start = 0;
	char *c1, *c2;
	while(start < S->len)
	{
		int k = 0;
		c1 = S->c + start;		//c1 , c2 均为地址, c1 为主串字符开始地址 
		c2 = T->c;				//c2 为字串 字符开始地址 
		if(S->len - start < T->len) return ERROR;
		while(*c1 == *c2 && k < T->len)
		{
			k++;
			c1++;
			c2++;
		}
		if(k >= T->len)		//若 K >= P->len 说明匹配成功, 进行删除 旧字串, 插入新 字串, 完成替换 
		{
			strDelete(S, start, T->len);
			strInsert(S, start, V);
			start += V->len;		//主串检索下标直接累加 新字串的 长度,等同于从原 主串的下一字符继续匹配 
		}
		else start++;		//否则 主串 检索下标累加 
	}
	return OK;
}

//将串  S 销毁, 已不能使用 
int strDestroy(String S)
{
	free(S);
	S = NULL;
	return OK;
}


//打印串 
void printStr(String S)
{
	char *c = S->c;
	*(c + S->len) = '\0'; 
	printf("%s\n", S->c);
}




