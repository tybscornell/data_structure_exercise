//初次使用Stack顺序栈 
#include<stdio.h>
#define MAXSIZE 1000
#define OK 1
#define ERROR 0
#define TRUE 1
#define FALSE 0
typedef struct{//顺序栈的定义 
	char c[MAXSIZE];
	int top;
}SeqStack;

void initStack(SeqStack *s);
int push(SeqStack *s, char c);
int pop(SeqStack *s, char *c);
int getTop(SeqStack *s, char *c);
int isTrue(char *temp, char c);
void outStack(SeqStack *s);

int main()
{
	SeqStack s, *s1 = &s;
	char str[MAXSIZE] = {"\0"};
	scanf("%s", str);
	initStack(s1);
	for(int i = 0; str[i] != '\0'; i++)
	{
//		outStack(s1);
		char b, *temp = &b;
		char c = str[i];
		switch(c)
		{
			case '(':
			case '[':
			case '{':
				push(s1, c);//压进栈内 
				break;
			case ')':
			case ']':
			case '}': 
				if(getTop(s1, temp) && isTrue(temp, c))	pop(s1, temp);
				else
				{
					printf("false\n");
					return 0;
				}
				break;
		}
	}
	if(s1->top != -1)
	{
		printf("false\n");
		return 0;
	}
	else printf("true\n");
	return 0;
}

//初始化顺序栈
void initStack(SeqStack *s)
{
	s->top = -1;
}

//顺序进栈运算
int push(SeqStack *s, char c)
{
	//将x置入s栈新栈顶
	if(s->top == MAXSIZE - 1)
	{
		printf("此顺序栈已存满数据!\n无法继续添加!\n");
		return FALSE;
	}
	s->top++;
	s->c[s->top] = c;
	return TRUE;
}

//顺序栈出栈运算
int pop(SeqStack *s, char *c)
{
	//将栈S栈顶元素读出,放到x所指的存储空间中,栈顶指针保持不变
	if(s->top < 0)
	{
		printf("此顺序栈栈顶已无数据!\n无法继续取出!\n");
		return FALSE;
	}
	*c = s->c[s->top];
	s->top--;
	return TRUE;
} 

//将栈顶元素读出
int getTop(SeqStack *s, char *a)
{
	if(s->top < 0)
	{
		printf("此顺序栈栈顶已无数据!\n无法继续取出!\n");
		return FALSE;
	}
	*a = s->c[s->top];
	return TRUE;
}

//判断括号是否匹配 
int isTrue(char *temp, char c)
{
	switch(*temp)
	{
		case '(':
			if(c == ')') return TRUE;
			else return FALSE;
			break;
		case '[':
			if(c == ']') return TRUE;
			else return FALSE;
			break;
		case '{':
			if(c == '}') return TRUE;
			else return FALSE;
			break;
		default:
			return FALSE;
			break;
	}
}

//输出栈内的所有元素 
//void outStack(SeqStack *s)
//{
//	printf("栈内数据如下：\n"); 
//	for(int i = 0; i <= s->top; i++)
//	{
//		printf("%c\n",s->c[i]);
//	}
//	printf("栈内top = %d\n", s->top);
//}
