#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#define LEN sizeof(Node)
#define MAXSIZE 100
#define OK 1
#define ERROR 0
#define TRUE 1
#define FALSE 0
//建立顺序栈 
typedef struct Node{
	int num[MAXSIZE];
	int top;
}*SeqStack;

//栈的有关操作函数 
void initSeqStack(SeqStack L);
int push(SeqStack L, int x);
int pop(SeqStack L ,int *x);
void printAllStack(SeqStack L);
int isEmply(SeqStack L);
int getTop(SeqStack L);

int main()
{
	SeqStack L;
	L = (Node *)malloc(LEN);
	while(true)
	{
		int n = 0, x = 0;
		printf("请选择：\n1.整数转化为二进制数\t2.二进制数转化为整数\t3.退出\n\n请输入：\t");
		scanf("%d", &n);
		if(n == 1)			//整数转化为二进制数 
		{
			int i = 0, temp;
			initSeqStack(L);
			printf("请输入这个整数：\n");
			scanf("%d", &x);
			temp = x;
			while(x != 0)
			{
				push(L, x%2);
				x /= 2;
				i++;
			}
			for(; i < 8; i++) push(L, 0);
			printf("转化成功!\n %d 二进制数为：\n", temp);
			printAllStack(L);	
		}
		else if(n == 2)			//二进制数转化为整数 
		{
			int sum = 0;
			printf("请输入二进制数(注意每个二进制数字之间用空格隔开)：\n");
			for(int i = 7; i >= 0; i--)
			{
				scanf("%d", &x);
				sum += x*pow(2, i);
			}
			printf("转化为整数为： %d \n", sum);
		}
		else break;
	}
	return 0;
}

void initSeqStack(SeqStack L)
{
	L->top = -1;
}

int push(SeqStack L, int x)
{
	if(L->top == MAXSIZE - 1)
	{
		printf("此栈已存满数据,无法继续存储!\n");
		return ERROR;
	}
	L->top++;
	L->num[L->top] = x; 
	return OK;
}

int pop(SeqStack L ,int *x)
{
	if(L->top == -1)
	{
		printf("此栈暂无存储数据,无法退栈!\n");
		return ERROR;
	}
	*x = L->num[L->top];
	L->top--;
	return OK;
}

void printAllStack(SeqStack L)
{
//	printf("栈内数据如下(由栈顶至栈底)：\n");
	for(int i = L->top; i >= 0; i--)
	printf("%d  ", L->num[i]);
	printf("\n");
}

int isEmply(SeqStack L)
{
	if(L->top == -1) return TRUE;
	else return FALSE;
}

int getTop(SeqStack L)
{
	return L->num[L->top];
}
