//操作说明：输入类似于    5+6+(2+44)*8-9%6+5^3#   等多项式求值,其中 '^' 代表指数 
#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#define LEN1 sizeof(OPTR)		//存储运算符栈的空间大小 
#define LEN2 sizeof(OVS)		//存储运算数的栈的空间大小 
#define MAXSIZE 100 
#define OK 1
#define ERROR 0
#define TRUE 1
#define FALSE 0

//运算符栈 
typedef struct OPTR{
	char c;
	struct OPTR *next;
}*optrLinkStack;
//运算数栈 
typedef struct OVS{
	float num;
	struct OVS *next;
}*ovsLinkStack;

//运算符栈的函数操作 
int optrInitStack(optrLinkStack top);		//初始化运算符栈 
int optrPush(optrLinkStack top, char c);		//压栈操作 
int optrPop(optrLinkStack top, char *c);		//退栈操作 
int getOptrTop(optrLinkStack top, char *c);		//取栈顶元素操作 
int isOptrEmply(optrLinkStack top);			//判断栈空操作 

//运算数栈的函数操作,同上 
int ovsInitStack(ovsLinkStack top);
int ovsPush(ovsLinkStack top, float x);
int ovsPop(ovsLinkStack top, float *x);
int getOvsTop(ovsLinkStack top, float *x);
int isOvsEmply(ovsLinkStack top);

int isOptr(char c);//判断一个字符是否为运算符
float calculation(float a, float b, char c);//求两个数的结果
char compare(char x, char y);//运算符的比较,据此可以得出多项式求解的先后次序 

//主函数 
int main()
{
	int n = 0;
	do{
		optrLinkStack optr = (OPTR *)malloc(LEN1);
		ovsLinkStack ovs = (OVS *)malloc(LEN2);
		optrInitStack(optr);
		ovsInitStack(ovs);					//开辟两个栈的空间并初始化 			  			
		float result = 0;//定义需要的变量
		char c = '\0', temp = '\0', c1 = '\0', c2 = '\0';
		printf("请输入多项式进行计算：(注意以'#'为结尾,多项式只支持运算符和数字,输入其它无效)\n");
		optrPush(optr, '#');	//压'#'开始符入栈 
		getOptrTop(optr, &temp);	//取得栈顶元素 
		setbuf(stdin, NULL);//使stdin输入流由默认缓冲区转为无缓冲区
		c = getchar();
		while(c != '#' || temp != '#')
		{
			if(isOptr(c))
			{	//若 c 为运算符则进行比较 
				switch(compare(c, temp))
				{
					case '~':
						//表明符号配对成功,即类似(),退栈,c读取下个字符 
						optrPop(optr, &c2);
						c = getchar();
						break;
					case '>':
					//表明新字符的优先级高,压栈 
						optrPush(optr, c);
						c = getchar();
						break;
					case '=':
					case '<':
					//新字符优先级小于等于栈顶运算符,进行计算 
						float a = 0, b = 0, re = 0;
						optrPop(optr, &c1);		//栈顶运算符出栈 
						ovsPop(ovs, &a);	//数据出栈 
						ovsPop(ovs, &b);	//数据出栈 
						re = calculation(a, b, c1);		//求结果 
						printf("Calculation is %.2f\n", re);	//输出本次所求结果 
						ovsPush(ovs, re);	//将结果压OVS运算栈 
						break;
				}
			}
			else
			{	//若不为运算符,将数字字符转化为数字压栈 
				int number = 0;
				while(c >= '0' && c <= '9')
				{				//将数字字符串转化为数字 
					number *=10;
					number += c - '0';
					c = getchar();
				}
				ovsPush(ovs, number);	//压OVS运算数栈 
			}
			getOptrTop(optr, &temp);	//temp继续为OPTR运算符栈的栈顶元素 
		}
		getOvsTop(ovs, &result);	//输出结果 
		printf("\nThe result is：\n%.2f\n", result);
		printf("请选择：\t1.继续求值\t2.退出\n");	//选择是否继续 
		scanf("%d", &n);
	}while(n == 1);
	return 0;
}


//判断是否为运算符 
int isOptr(char c)
{
	switch(c)
	{	//若此字符为这些字符表示是	操作运算符 
		case '+':
		case '-':
		case '*':
		case '/':
		case '%':
		case '^':
		case '(':
		case ')':
		case '#':
			return TRUE;
		default:		//否则不是 
			return FALSE;
	}
}

//求结果函数 
float calculation(float a, float b, char c)
{
	switch(c)
	{		//因为栈的存放是逆序的,运算时要反向计算 
		case '+': return b + a;
		case '-': return b - a;
		case '*': return b * a;
		case '/': return b / a;
		case '%': return (int)b % (int)a;
		case '^': return pow(b, a);
		default:
			printf("出现错误!\n");
			return 0;
	}
}

//运算符的比较 函数 
char compare(char x, char y)		
{			//其中x为新读取的运算字符,y为OPTR栈顶运算符 
	switch(x)
	{			//根据算数优先级获得优先级次序 
		case '+':
			switch(y)
			{
				case '+':return '=';
				case '-':return '=';
				case '*':return '<';
				case '/':return '<';
				case '%':return '<';
				case '^':return '<';
				case '(':return '>';
				case '#':return '>';
			}
		case '-':
			switch(y)
			{
				case '+':return '=';
				case '-':return '=';
				case '*':return '<';
				case '/':return '<';
				case '%':return '<';
				case '^':return '<';
				case '(':return '>';
				case '#':return '>';
			}
		case '*':
			switch(y)
			{
				case '+':return '>';
				case '-':return '>';
				case '*':return '=';
				case '/':return '=';
				case '%':return '=';
				case '^':return '<';
				case '(':return '>';
				case '#':return '>';
			}
		case '/':
			switch(y)
			{
				case '+':return '>';
				case '-':return '>';
				case '*':return '=';
				case '/':return '=';
				case '%':return '=';
				case '^':return '<';
				case '(':return '>';
				case '#':return '>';
			}
		case '%':
			switch(y)
			{
				case '+':return '>';
				case '-':return '>';
				case '*':return '=';
				case '/':return '=';
				case '%':return '=';
				case '^':return '<';
				case '(':return '>';
				case '#':return '>';
			}
		case '^':
			switch(y)
			{
				case '+':return '>';
				case '-':return '>';
				case '*':return '>';
				case '/':return '>';
				case '%':return '>';
				case '^':return '>';
				case '(':return '>';
				case '#':return '>';
			}
		case '(':
			switch(y)
			{
				case '+':return '>';
				case '-':return '>';
				case '*':return '>';
				case '/':return '>';
				case '%':return '>';
				case '^':return '>';
				case '(':return '>';
				case '#':return '>';
			}
		case ')':
			switch(y)
			{
				case '+':return '<';
				case '-':return '<';
				case '*':return '<';
				case '/':return '<';
				case '%':return '<';
				case '^':return '<';
				case '(':return '~';
				case '#':return '>';
			}
		case '#':
			switch(y)
			{
				case '+':return '<';
				case '-':return '<';
				case '*':return '<';
				case '/':return '<';
				case '%':return '<';
				case '^':return '<';
				case '(':return '<';
				case '#':return '~';
			}
	}
}
//以下5个函数为字符运算符栈OPTR的操作函数 
//运算符栈初始化操作 
int optrInitStack(optrLinkStack top)
{
	top->next = NULL;
	return OK;
}

//运算符栈压栈操作 
int optrPush(optrLinkStack top, char c)
{
	struct OPTR *s = (OPTR *)malloc(LEN1);
	s->c = c;
	s->next = top->next;
	top->next = s;
	return OK;
}

//运算符栈退栈操作 
int optrPop(optrLinkStack top, char *c)
{
	struct OPTR *p = top->next;
	if(p == NULL)
	{
		printf("链栈已空,无法继续退栈!\n");
		return ERROR;
	}
	*c = p->c;
	top->next = p->next;
	free(p);
	return OK;
}

//运算符栈取得栈顶元素操作 
int getOptrTop(optrLinkStack top, char *c)
{
	if(top->next == NULL)
	{
		printf("链栈已空,无法获取栈顶元素!\n");
		return ERROR;
	}
	*c = top->next->c;
	return OK;
}

//判断栈空操作 
int isOptrEmply(optrLinkStack top)
{
	if(top->next == NULL) return TRUE;
	else return FALSE;
}

//以下5个函数为字符运算数栈OVS的操作函数,同上 
int ovsInitStack(ovsLinkStack top)
{
	top->next = NULL;
	return OK;
}

int ovsPush(ovsLinkStack top, float x)
{
	struct OVS *s = (OVS *)malloc(LEN2);
	s->num = x;
	s->next = top->next;
	top->next = s;
	return OK;
}

int ovsPop(ovsLinkStack top, float *x)
{
	struct OVS *p = top->next;
	if(p == NULL)
	{
		printf("此栈已空,无法继续退栈!\n");
		return ERROR;
	}
	*x = p->num;
	top->next = p->next;
	free(p);
	return OK;
}

int getOvsTop(ovsLinkStack top, float *x)
{
	if(top->next == NULL)
	{
		printf("此栈已空,无法取出元素!\n");
		return ERROR;
	}
	*x = top->next->num;
	return OK;
}

int isOvsEmply(ovsLinkStack top)
{
	if(top->next == NULL) return TRUE;
	else return FALSE;
}

