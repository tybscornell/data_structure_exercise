#include<stdio.h>
#include<stdlib.h>
#define LEN sizeof(Node)
#define OK 1
#define ERROR 0
#define TRUE 1
#define FALSE 0;

typedef struct Node{
	char c;
	Node *next;
}LinkQueueNode;

typedef struct{
	LinkQueueNode *front;
	LinkQueueNode *rear;
}LinkQueue;

//链队列的初始化
int initQueue(LinkQueue *Q)
{
	//将Q初始化为一个空的队列
	Q->front = (Node *)malloc(LEN);
	if(Q->front != NULL)
	{
		Q->rear = Q->front;
		Q->rear->next = NULL;
		return OK;
	}
	else return ERROR;
}

//链队列入队的算法
int enterQueue(LinkQueue *Q, char c)
{
	Node *s = (Node *)malloc(LEN);
	if(s != NULL)
	{
		s->c = c;
		s->next = NULL;
		Q->rear->next = s;
		Q->rear = s;
		return OK;
	}
	else return  ERROR;
}

//链队列出队的算法
int deleteQueue(LinkQueue *Q, char *c)
{
	if(Q->front == Q->rear) return ERROR;
	Node *p = Q->front->next;
	*c = p->c;
	Q->front->next = p->next;
	if(p == Q->rear)
	{
		Q->rear = Q->front;
	}
	free(p);
	return OK;
}

//链队列判空的函数
int isEmply(LinkQueue *Q)
{
	if(Q->front == Q->rear) return TRUE;
	else return FALSE;
}

//获取链队的队首元素的算法
int getHead(LinkQueue *Q, char *c)
{
	if(Q->front == Q->rear) return ERROR;
	*c = Q->front->next->c;
	return OK;
}

//从队首至队尾输出链队的所有元素
int  printQueue(LinkQueue *Q)
{
	Node *q = Q->front->next;
	while(q != NULL)
	{
		printf("%c\n",q->c);
		q = q->next;
	}
}
