#include<stdio.h>
#include<stdlib.h>
#include"charStack.h"
int main()
{
	int n = 0;
	LinkStack top;
	printf("请输入以@结束的一串字符(序列1&序列2@,序列1,2均不含&,输入正确格式程序才能正确运行)\n");
	printf("例如:  a+b&b+a@ 是回文字符串, 1+3&3-1@ 不是回文字符串\n"); 
	do{
		char c = '\0', temp = '\0';
		top = (LinkStack)malloc(sizeof(LinkStack));
		initStack(top);				//初始化字符栈 
		printf("输入字符串:\n");
		fflush(stdin);
		c = getchar();
		while(c != '&')
		{	//未检索到 '&' 字符, 累计入栈 
			push(top, c);
			c = getchar();
		}	
		c = getchar();		//隔过 '&' 字符读取下一个字符和栈内元素依次比较,由栈顶至栈底,不匹配则推出 
		pop(top, &temp);	//检索到 '&' 字符后累计出栈 
		while(c == temp)
		{
			c = getchar();
			pop(top, &temp);
		}
		if(c == '@') printf("String is Palindrome string!\n");		//若检索至 '@' 符,代表此字符串是回文的 
		else printf("String not is Palindrome string!\n");		//否则不是
		fflush(stdin);
		printf("请选择:\t1.继续判断\t2.退出\n\n请输入:\t");
		scanf("%d", &n);
	}while(n == 1);
	return 0;
}
