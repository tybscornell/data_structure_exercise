#include<stdio.h>
#include<stdlib.h>
#define LEN sizeof(Node)
#define OK 1
#define ERROR 0
#define TRUE 1
#define FALSE 0

//�ַ�ջ�Ĳ������� 
typedef struct Node{
	char c;
	Node *next;
}*LinkStack;

//��ʼ���ַ���ջ
int initStack(LinkStack top)
{
	top->next = NULL;
	return OK;
}

//ѹջ����
int push(LinkStack top, char c)
{
	Node *s = (Node *)malloc(LEN);
	s->c = c;
	s->next = top->next;
	top->next = s;
	return OK;
} 

//��ջ���� 
int pop(LinkStack top, char *c)
{
	if(top->next == NULL) return ERROR;
	Node *p = top->next;
	*c = p->c;
	top->next = p->next;
	free(p);
	return OK;
}

//ȡ��ջ��Ԫ�� 
int getTop(LinkStack top, char *c)
{
	if(top->next == NULL)
	{
		printf("GetTop is error,Stack is empty!\n");
		return ERROR;
	}
	*c = top->next->c;
	return OK;
}

//�ж�ջ��
int isEmply(LinkStack top)
{
	if(top->next == NULL) return TRUE;
	else return FALSE;
}

//���ջ������Ԫ��
void printStack(LinkStack top)
{
	Node *p = top->next;
	while(p != NULL)
	{
		printf("%c\n", p->c);
		p = p->next;
	}
}
