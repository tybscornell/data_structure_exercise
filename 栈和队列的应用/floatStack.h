#include<stdio.h>
#include<stdlib.h>
#define LEN sizeof(Node)
#define OK 1
#define ERROR 0
#define TRUE 1
#define FALSE 0

//浮点数栈的操作函数 
typedef struct Node{
	float x;
	Node *next;
}*LinkStack;

//初始化浮点型栈
int initStack(LinkStack top)
{
	top->next = NULL;
	return OK;
}

//压栈操作
int push(LinkStack top, float x)
{
	Node *s = (Node *)malloc(LEN);
	s->x = x;
	s->next = top->next;
	top->next = s;
	return OK;
} 

//退栈操作 
int pop(LinkStack top, float *x)
{
	if(top->next == NULL) return ERROR;
	Node *p = top->next;
	*x = p->x;
	top->next = p->next;
	free(p);
	return OK;
}

//取得栈顶元素 
int getTop(LinkStack top, float *x)
{
	if(top->next == NULL)
	{
		printf("GetTop is error,Stack is empty!\n");
		return ERROR;
	}
	*x = top->next->x;
	return OK;
}

//判断栈空
int isEmply(LinkStack top)
{
	if(top->next == NULL) return TRUE;
	else return FALSE;
}

//输出栈内所有元素
void printStack(LinkStack top)
{
	Node *p = top->next;
	while(p != NULL)
	{
		printf("%x\n", p->x);
		p = p->next;
	}
}
