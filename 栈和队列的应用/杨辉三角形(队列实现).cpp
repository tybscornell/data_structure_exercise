#include<stdio.h>
#include"intQueue.h"
#include<stdlib.h>
int main()
{
	int n = 0, temp = 0, x = 0;
	LinkQueue *Q = (LinkQueue *)malloc(sizeof(LinkQueue));
	printf("请输入一个数 n 打印杨辉三角形:\n");
	scanf("%d", &n);
	initQueue(Q);		//初始化队列 
	enterQueue(Q, 1);		//第一行 1 入队 
	for(int j = 2; j <= n; j++)
	{					//每次循环打印出j-1行的队列,生成新的第j行队列 
		enterQueue(Q, 1);
		for(int i = 1; i <= n - j + 1; i++) printf(" ");
		for(int i = 1; i <= j-2; i++)		
		{				////生成j行中间数据并打印j-1行的j-1个数据 
			deleteQueue(Q, &temp);
			printf("%d ", temp);
			getHead(Q, &x);
			temp += x;		//生成新数据并令其入队 
			enterQueue(Q, temp);
		}	
		deleteQueue(Q, &temp);
		printf("%d\n", temp);
		enterQueue(Q, 1);
	}
	while(!isEmply(Q))
	{
		deleteQueue(Q, &temp);
		printf("%d ", temp);
	}
	return 0;
}
