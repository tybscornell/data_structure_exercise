#include<stdio.h>
#include<stdlib.h>
#define TRUE 1
#define FALSE 0
#define OK 1
#define ERROR 0
#define MAX_VERTEX_NUM 50		//定义图的最多顶点个数
#define INFINITY 32768			//表示最大值  ∞

int visited[MAX_VERTEX_NUM];
//表示图的种类: DG表示有向图, DN表示有向网, UDG表示无向图, UDN表示无向网 
typedef enum {DG, DN, UDG, UDN} GraphKind;
typedef char TypeElem;

//对于无权图, 用 1 或 0 表示是否相邻; 对带权图, 则为权值类型
//存储顶点关系的二维数组元素类型 
struct ArcNode{
	int flag;					//1代表两顶点之间具有相邻关系, 否则无关联 
	int length;					//若为带权图, length则表示权值 
};

//存储图的数据类型 
typedef struct GraphNode{
	TypeElem vertex[MAX_VERTEX_NUM];						//存储图的顶点的数组 
	struct ArcNode arcs[MAX_VERTEX_NUM][MAX_VERTEX_NUM];	//存储顶点之间关系的二维数组 
	int vexnum, arcnum;										//图的顶点数和弧数 
	GraphKind kind; 										//图的类型
}*AdjMatrix;

void createGraph(AdjMatrix graph);			//创建图 
void createGraphUDG(AdjMatrix graph);		//创建无向图 
void createGraphDG(AdjMatrix graph);		//创建有向图 
void createGraphUDN(AdjMatrix graph);		//创建无向网 
void createGraphDN(AdjMatrix graph);		//创建有向网 
AdjMatrix destoryGraph(AdjMatrix graph);			//销毁图 
int locateVertex(AdjMatrix graph, TypeElem data);		//根据顶点得到顶点序号 
TypeElem getVertex(AdjMatrix graph, int i);				//根据顶点序号获得顶点 
TypeElem firstAdjVertex(AdjMatrix graph, TypeElem data);	//得到某顶点的第一个邻接点 
TypeElem nextAdjVertex(AdjMatrix graph, TypeElem v, TypeElem w);	//得到v 的邻接点w 后的最近邻接点 
void insertVertex(AdjMatrix graph, TypeElem u);		//插入顶点
void insertArc(AdjMatrix graph, TypeElem v, TypeElem w);	//插入一条从顶点 v 到顶点 w 的一条弧
void deleteArc(AdjMatrix graph, TypeElem v, TypeElem w);	//删除一条从顶点 v 到顶点 w 的一条弧
void deleteVertex(AdjMatrix graph, TypeElem v);		//删除顶点及关于此顶点的所有信息
void TraverseGraph(AdjMatrix graph);				//遍历此图
void DepthFirstSearch(AdjMatrix graph, int i); 		//深度优先(DFS)遍历此图
void depthFirstSearch(AdjMatrix graph, int i) 		//采用邻接矩阵的 DepthFirstSearch的深度优先遍历 
void printGraph(AdjMatrix graph);					//打印此图的顶点表 及 关系表 

void createGraph(AdjMatrix graph)
{
	int gkind;
	printf("请输入图的类型:\n1.无向图\t2.有向图\n3.无向网\t4.有向网\n");
	scanf("%d", &gkind);
	while(gkind < 1 || gkind > 4)
	{
		printf("输入有误, 请再次输入:\n");
		scanf("%d", &gkind);
	}
	printf("请输入图的顶点数和弧数:\n");
	scanf("%d%d", &graph->vexnum, &graph->arcnum);
	//初始化图中顶点的关系 
	for(int i = 0; i < graph->vexnum; i++)
	{
		for(int j = 0; j < graph->vexnum; j++)
		{
			graph->arcs[i][j].flag = 0;
			graph->arcs[i][j].length = INFINITY;
		}
	}
	printf("请输入所有顶点元素的值:\n");
	for(int i = 0; i < graph->vexnum; i++)
	{
		char c;
		scanf("%c", &c);
		if(c != ' ' && c != '\n')
		graph->vertex[i] = c;
        else i--;
	}
	switch(gkind)
	{
		case 1:
			graph->kind = UDG;
			createGraphUDG(graph); 
			break;
		case 2:
			graph->kind = DG;
			createGraphDG(graph);
			break;
		case 3:
			graph->kind = UDN;
			createGraphUDN(graph);
	        break;
        case 4:
        	graph->kind = DN;
        	createGraphDN(graph);
        	break;
	}
	puts("创建图成功!");
}

//创建无向图
void createGraphUDG(AdjMatrix graph)
{
	int vertex_x, vertex_y;
	printf("请输入所有弧的信息:\n\n");
	for(int i = 0; i < graph->arcnum; i++)
	{
		TypeElem vex[2];
		printf("请输入第 %d 条弧的两个顶点序号:\n", i+1);
        for(int j = 0; j < 2; j++)
        {
        	char c;
        	scanf("%c", &c);
        	if(c == ' ' || c == '\n') j--;
        	else vex[j] = c;
		}
		vertex_x = locateVertex(graph, vex[0]);
		vertex_y = locateVertex(graph, vex[1]);
		graph->arcs[vertex_x-1][vertex_y-1].flag = 1;
		graph->arcs[vertex_y-1][vertex_x-1].flag = 1;
	}
}

//创建有向图
void createGraphDG(AdjMatrix graph)
{
    int vertex_x, vertex_y;
	puts("请输入所有弧的信息:\n\n");
	setbuf(stdin, NULL);
	for(int i = 0; i < graph->arcnum; i++)
	{
		TypeElem vex[2];
		printf("请输入第 %d 条弧的两个顶点序号(以方向为次序):\n", i+1);
        for(int j = 0; j < 2; j++)
        {
        	char c;
        	scanf("%c", &c);
        	if(c == ' ' || c == '\n') j--;
        	else vex[j] = c;
		}
		vertex_x = locateVertex(graph, vex[0]);
		vertex_y = locateVertex(graph, vex[1]);
		graph->arcs[vertex_x-1][vertex_y-1].flag = 1;
	}
}

//创建无向网
void createGraphUDN(AdjMatrix graph)
{
	int vertex_x, vertex_y, weight;
	puts("请输入所有弧的信息:\n\n");
	setbuf(stdin, NULL);
	for(int i = 0; i < graph->arcnum; i++)
	{
		TypeElem vex[2];
		printf("请输入第 %d 条弧的两个顶点序号及权值:\n", i+1);
        for(int j = 0; j < 2; j++)
        {
        	char c;
        	scanf("%c", &c);
        	if(c == ' ' || c == '\n') j--;
        	else vex[j] = c;
		}
		scanf("%d", &weight);
		vertex_x = locateVertex(graph, vex[0]);
		vertex_y = locateVertex(graph, vex[1]);
		graph->arcs[vertex_x-1][vertex_y-1].flag = 1;
		graph->arcs[vertex_x-1][vertex_y-1].length = weight;
		graph->arcs[vertex_y-1][vertex_x-1].flag = 1;
		graph->arcs[vertex_y-1][vertex_x-1].length = weight;
	}
}

//创建有向网
void createGraphDN(AdjMatrix graph)
{
    int vertex_x, vertex_y, weight;
	puts("请输入所有弧的信息:\n\n");
	setbuf(stdin, NULL);
	for(int i = 0; i < graph->arcnum; i++)
	{
		TypeElem vex[2];
		printf("请输入第 %d 条弧的两个顶点序号(以方向为次序)及权值:\n", i+1);
        for(int j = 0; j < 2; j++)
        {
        	char c;
        	scanf("%c", &c);
        	if(c == ' ' || c == '\n') j--;
        	else vex[j] = c;
		}
		scanf("%d", &weight);
		vertex_x = locateVertex(graph, vex[0]);
		vertex_y = locateVertex(graph, vex[1]);
		graph->arcs[vertex_x-1][vertex_y-1].flag = 1;
		graph->arcs[vertex_x-1][vertex_y-1].length = weight;
	}
}


//销毁图 G  
AdjMatrix destoryGraph(AdjMatrix graph)
{
	free(graph);
	graph = NULL;
	puts("此图已被成功销毁!");
	return graph;
}


//返回顶点 v 在图 G 中的位置
int locateVertex(AdjMatrix graph, TypeElem data)
{
	int i = 0;
	while(graph->vertex[i] != data && i < graph->vexnum)
	i++;
	if(i == graph->vexnum) return 0;	//未找到顶点  v, 返回 0 
	else return i+1;					//找到顶点 v, 返回 位置序号 
}


//返回图 G 中的第 i 个顶点的值 
TypeElem getVertex(AdjMatrix graph, int i)
{ 
	return graph->vertex[i-1];		//返回第 i 个顶点的值	
}


//返回图 G 中顶点 v 的第一个邻接点。若 v 无邻接点或图 G 中无顶点v ,则函数返回空 
TypeElem firstAdjVertex(AdjMatrix graph, TypeElem data)
{
	int i;
	i = locateVertex(graph, data);
	if(i == 0)
	{
		puts("图 G 中暂无此顶点, 所以无法找到它的邻接点!");
		return NULL;
	}
	for(int j = 0; j < graph->vexnum; j++)
	{
		if(graph->arcs[i-1][j].flag == 1)
		return getVertex(graph, j+1);
	}
	puts("在图 G 中, 此顶点暂无邻接点!");
	return NULL;
}


//返回顶点 v 的下一个邻接点(紧跟在 w 后面)。若 w 是 v 的最后一个邻接点, 则函数返回空 
TypeElem nextAdjVertex(AdjMatrix graph, TypeElem v, TypeElem w)
{
	int i, j;
	i = locateVertex(graph, v);
	j = locateVertex(graph, w);
	for(int k = j; k < graph->vexnum; k++)
	{
	    if(graph->arcs[i-1][k].flag == 1)
		return getVertex(graph, k+1);	
    }
    puts("无法找到下一个邻接点, 已是最后一个邻接点!");
    return NULL;
}


//在图 G 中增加一个顶点 u 。
void insertVertex(AdjMatrix graph, TypeElem u)
{
	if(u == NULL)
	{
		puts("此顶点为空, 请重试!");
		return;
	}
	graph->vertex[graph->vexnum] = u;
	for(int i = 0; i <= graph->vexnum; i++)
	{
		graph->arcs[graph->vexnum][i].flag = 0;
		graph->arcs[graph->vexnum][i].length = INFINITY;
		graph->arcs[i][graph->vexnum].flag = 0;
		graph->arcs[i][graph->vexnum].length = INFINITY;
	}
	graph->vexnum++;
}


//删除图 G 的顶点 v及与顶点 v 相关联的弧
void deleteVertex(AdjMatrix graph, TypeElem v)
{
	int i;
    if(v == NULL)
	{
	    puts("此顶点为空, 无法进行删除!");
		return;
	}
	i = locateVertex(graph, v)-1;
	
        //判断顶点的删除位置
	//顶点位于最后一个元素的删除方式 
	if(i == graph->vexnum-1)
	{
		graph->vertex[i] = NULL;
	}
	//顶点位于中间元素的删除方式 
	else
	{
		for(int j = i; j < graph->vexnum-1; j++)
		{
			graph->vertex[j] = graph->vertex[j+1];
		}
		graph->vertex[graph->vexnum-1] = NULL;
	}
	
	//对存储顶点的矩阵进行调整修改
	//删除 此顶点所在行号的所有关系 
	for(int j = i; j < graph->vexnum-1; j++)
	{
	    for(int k = 0; k < graph->vexnum; k++)
		{
		    graph->arcs[j][k] = graph->arcs[j+1][k];	
		}
    }
    //删除 此顶点所在列号的所有关系 
    for(int j = i; j < graph->vexnum-1; j++)
	{
	    for(int k = 0; k < graph->vexnum; k++)
		{
		    graph->arcs[k][j] = graph->arcs[k][j+1];	
		}
    }
	//清除多余的矩阵数据 
	for(int j = 0; j < graph->vexnum; j++)
	{
		graph->arcs[j][graph->vexnum-1].flag = 0;
		graph->arcs[j][graph->vexnum-1].length = INFINITY;
		graph->arcs[graph->vexnum-1][j].flag = 0;
		graph->arcs[graph->vexnum-1][j].length = INFINITY;
	}
	graph->vexnum--;
}


//在图 G 中增加一条从顶点 v 到顶点 w 的弧(仅针对于无向图与无向网)
void insertArc(AdjMatrix graph, TypeElem v, TypeElem w)
{
	int vertex_x, vertex_y;
	vertex_x = locateVertex(graph, v);
	vertex_y = locateVertex(graph, w);
	graph->arcs[vertex_x-1][vertex_y-1].flag = 1;
	graph->arcs[vertex_y-1][vertex_x-1].flag = 1;
}


//删除图 G 中从顶点 v 到顶点 w 的一条弧(仅针对无向图与无向网)
void deleteArc(AdjMatrix graph, TypeElem v, TypeElem w)
{
	int vertex_x, vertex_y;
	vertex_x = locateVertex(graph, v);
	vertex_y = locateVertex(graph, w);
	graph->arcs[vertex_x-1][vertex_y-1].flag = 0;
	graph->arcs[vertex_x-1][vertex_y-1].length = INFINITY;
	graph->arcs[vertex_y-1][vertex_x-1].flag = 0;
	graph->arcs[vertex_y-1][vertex_x-1].length = INFINITY;
}


//深度优先搜索 (DFS)遍历此图
void TraverseGraph(AdjMatrix graph)
{
	//在图 graph 中寻找未被访问的顶点作为起始点, 并调用深度优先搜素过程进行遍历。Graph 表示图的一种存储结构, 如邻接矩阵或邻接表等
	if(graph == NULL)
	{
		printf("此图未生成, 无法进行遍历!\n");
		return;
	}
	for(int i = 0; i < graph->vexnum; i++) visited[i] = FALSE;		//初始化访问标志数组
	for(int i = 0; i < graph->vexnum; i++)
	if(!visited[i]) DepthFirstSearch(graph, i); 
}


//深度优先遍历序号 i 所在的连通图
void DepthFirstSearch(AdjMatrix graph, int i)
{
	//深度遍历 i 所在的连通图
	int w;
	TypeElem c;
	printf("%c\n", graph->vertex[i]);
	visited[i] = TRUE;												//访问顶点, 并访问标志数组相应分量值
	c = firstAdjVertex(graph, graph->vertex[i]);
	w = locateVertex(graph, c) -1;
	while(w != -1)
	{
		if(!visited[w]) DepthFirstSearch(graph, w);
		c = nextAdjVertex(graph, getVertex(graph, i+1), getVertex(graph, w+1));
		w = locateVertex(graph, c)-1;
	}
}


//采用邻接矩阵的 DepthFirstSearch的深度优先遍历 
void depthFirstSearch(AdjMatrix graph, int i)
{
    //图 G 为邻接矩阵类型 AdjMatRix
	printf("%c\n", graph->vertex[i]);
	for(int j = 0; j < graph->vexnum; j++)
	{
	    if(!visited[j] && graph->arcs[i][j].flag == 1)
		depthFirstSearch(graph, j);	
	}
}


//打印图中的所有顶点及顶点关系的矩阵 
void printGraph(AdjMatrix graph)
{
	if(graph == NULL)
	{
		puts("此图为空图!");
		return;
	}
	puts("\n顶点及其位置关系如下:");
	for(int i = 0; i < graph->vexnum; i++) printf("%c  ", graph->vertex[i]);
	puts("");
	for(int i = 0; i < graph->vexnum; i++) printf("%d  ", i+1);
	puts("\n顶点关系矩阵图如下:");
	for(int i = 0; i < graph->vexnum; i++)
	{
		for(int j = 0; j < graph->vexnum; j++)
		{
			printf("%d   ", graph->arcs[i][j].flag);
		}
		printf("\n\n");
	}
}



