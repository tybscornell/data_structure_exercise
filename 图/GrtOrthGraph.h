#include<stdio.h>
#include<stdlib.h>
#define MAX_VERTEX_NUM 100
typedef enum{DG, DN, UDG, UDN} GraphKind;		//定义枚举变量 GraphKind进行对图的选择 
typedef char TypeElem;							//定义顶点数据信息为字符型 

//定义弧结点 结构体 
typedef struct Arc_Node{
	int tailvex, headvex;						//定义弧尾和弧头 
	struct Arc_Node *hlink, *tlink;				//定义指向以此弧为弧头，弧尾的下一条弧的指针 
}*ArcNode;

//定义顶点结点 结构体 
typedef struct Vex_Node{
	TypeElem data;								//定义顶点的信息 
	Arc_Node *firstin, *firstout;				//定义以此顶点的为弧头, 弧尾的第一条弧 
}*VexNode;

//定义图的数据结构结构体 
typedef struct{
	Vex_Node vertex[MAX_VERTEX_NUM];			//存储顶点的一维数组 
	int vexnum, arcnum;							//记录顶点的个数和弧的个数 
	GraphKind kind;								//定义此图的类型 
} *OrthList;


//以十字链表为存储图的数据结构, 完成图的创建 
//此程序仅供参考, 不可使用, 图的操作方法具体参考邻接矩阵存储和邻接表存储的操作算法 
void creatOrthList(OrthList graph)
{
	printf("请输入图的顶点个数和图的弧的个数:\n");
	scanf("%d%d", &graph->vexnum, &graph->arcnum);
	for(int i = 0; i < graph->vexnum; i++)
	{
		scanf("%c", &graph->vertex[i].data);
		graph->vertex[i].firstin = NULL;
		graph->vertex[i].firstout = NULL;
	}
	for(int k = 0; k < graph->arcnum; k++)
	{
		int i, j;
		char c1, c2;
		ArcNode p; 
		printf("请输入第 %d 条弧的两个顶点:\n", k+1);
		scanf("%c,%c", &c1, &c2);
		i = LocateVertex(graph, c1);
		j = LocateVertex(graph, c2);
		p = (ArcNode)malloc(sizeof(ArcNode));
		p->tailvex = i;
		p->headvex = j;
		p->tlink = graph->vertex[i].firstout;
		graph->vertex[i].firstout = p;
		p->hlink = graph->vertex[j].firstin;
		graph->vertex[j].firstin = p;
	}
}
