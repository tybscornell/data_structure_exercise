#include<stdio.h>
#include<stdlib.h>
#define MAX_VERTEX_NUM 20
#define LEN sizeof(AdjList)
#define NODE sizeof(ArcNode)

typedef enum{DG, DN, UDG, UDN} GraphKind;
typedef char TypeElem;


//定义构成弧的元素顶点的元素 
typedef struct ArcNode{
	int adjvex;
	int length;
	struct ArcNode *next;
}*Node;

//定义表头结点表存储顶点的表头结点元素 
typedef struct VertexNode{
	TypeElem data;
	ArcNode *firstarc;
}VertexNode;

//定义邻接表存储所有顶点及它们的关系 
typedef struct List{
	VertexNode vertex[MAX_VERTEX_NUM];
	int vexnum, arcnum;
	GraphKind kind;
}*AdjList;

void createAdjList(AdjList graph);			//创建邻接表生成图 
void createAdjListUDG(AdjList graph);		//创建无向图 
void createAdjListDG(AdjList graph);		//创建有向图 
void createAdjListUDN(AdjList graph);		//创建无向网 
void createAdjListDN(AdjList graph);		//创建有向网 
AdjList destoryGraph(AdjList graph);		//销毁图 
int locateVertex(AdjList graph, TypeElem data);			//根据顶点值取得顶点所在表头结点表的位置 
TypeElem getVertex(AdjList graph, int i);				//根据顶点所在位置得到顶点的值 
TypeElem firstAdjVertex(AdjList graph, TypeElem data);	//得到顶点 data的第一个邻接点 
TypeElem nextAdjVertex(AdjList graph, TypeElem v, TypeElem w);		//得到顶点v 的邻接点 w后的第一个邻接点 
void insertVertex(AdjList graph, TypeElem u);			//为图插入一个顶点 
void deleteVertex(AdjList graph, TypeElem v);			//删除图的一个顶点及其对应的弧 
void insertArc(AdjList graph, TypeElem v, TypeElem w);	//根据顶点 v 和顶点 w插入一条弧 
void deleteArc(AdjList graph, TypeElem v, AdjList w);	//删除顶点 v 和顶点 w之间的一条弧 
void printGraph(AdjList graph);							//打印此图的邻接表 

//创建邻接表 
void createAdjList(AdjList graph)
{
	int gkind;
	//输入图的类型
	puts("请输入图的类型:\n1.无向图\t2.有向图\n3.无向网\t4.有向网\n");
	scanf("%d", &gkind);
	while(gkind < 1 || gkind > 4)
	{
		puts("输入有误, 请重新输入!");
		scanf("%d", &gkind);
	}
	//输入图的顶点数和弧数 
	puts("请输入此图的顶点数与弧数:");
	scanf("%d%d", &graph->vexnum, &graph->arcnum);
	//输入图的所有顶点
	for(int i = 0; i < graph->vexnum; i++) graph->vertex[i].firstarc = NULL;
	puts("请输入此图的所有顶点:");
	for(int i = 0; i < graph->vexnum; i++)
	{
		char c;
		scanf("%c", &c);
		if(c == ' ' || c == '\n') i--;
		else
		graph->vertex[i].data = c;
	}
	//根据图的类型选择操作 
	switch(gkind)
	{
        case 1:
        	graph->kind = UDG;
        	createAdjListUDG(graph);
        	break;
       	case 2:
        	graph->kind = DG;
        	createAdjListDG(graph);
        	break;
       	case 3:
        	graph->kind = UDN;
        	createAdjListUDN(graph);
        	break;
       	case 4:
        	graph->kind = DN;
        	createAdjListDN(graph);
        	break;
	}
	puts("此图创建成功!");
}


//创建无向图
void createAdjListUDG(AdjList graph)
{
    for(int i = 0; i < graph->vexnum; i++)
	{
		int j = 1;
		int next_vertex;
	    do{
	    	ArcNode *q;
    		q = (ArcNode *)malloc(NODE);
	    	printf("请输入第 %d 个顶点 %c 的第 %d 个邻接点的序号(若无,输入 0):\n", i+1, getVertex(graph, i+1), j);
	    	scanf("%d", &next_vertex);
	    	//采用头插法进行链表的连接 
	    	if(next_vertex != 0)
	    	{
	    		q->adjvex = next_vertex;
	    		q->next = graph->vertex[i].firstarc;
	    		graph->vertex[i].firstarc = q;
			}
			j++;
		}while(next_vertex != 0);
	}
}


//创建有向图
void createAdjListDG(AdjList graph)
{
    for(int i = 0; i < graph->vexnum; i++)
	{
		int j = 1;
		int next_vertex;
	    do{
	    	ArcNode *q;
	    	printf("请输入第 %d 个顶点 %c 的第 %d 个邻接点(此邻接点为终止点,即有序)的序号(若无,输入 0):\n", i+1, getVertex(graph, i+1), j);
	    	scanf("%d", &next_vertex);
	    	//采用头插法进行链表的连接 
	    	if(next_vertex != 0)
	    	{
	    		q = (ArcNode *)malloc(NODE);
	    		q->adjvex = next_vertex;
	    		q->next = graph->vertex[i].firstarc;
	    		graph->vertex[i].firstarc = q;
			}
			j++;
		}while(next_vertex != 0);
	}
}


//创建无向网 
void createAdjListUDN(AdjList graph)
{
    for(int i = 0; i < graph->vexnum; i++)
	{
		int j = 1;
		int next_vertex;
	    do{
	    	ArcNode *q;
	    	printf("请输入第 %d 个顶点 %c 的第 %d 个邻接点的序号(若无,输入 0):\n", i+1, getVertex(graph, i+1), j);
	    	scanf("%d", &next_vertex);
	    	//采用头插法进行链表的连接 
	    	if(next_vertex != 0)
	    	{
	    		q = (ArcNode *)malloc(NODE);
	    		puts("请输入它们所形成弧的权值:");
	    		scanf("%d", &q->length);
	    		q->adjvex = next_vertex;
	    		q->next = graph->vertex[i].firstarc;
	    		graph->vertex[i].firstarc = q;
			}
			else break;
			j++;
		}while(1);
	}
}


//创建有向网 
void createAdjListDN(AdjList graph)
{
    for(int i = 0; i < graph->vexnum; i++)
	{
		int j = 1;
		int next_vertex;
	    do{
	    	ArcNode *q;
	    	printf("请输入第 %d 个顶点 %c 的第 %d 个邻接点(此邻接点为终止点, 即有序)的序号(若无,输入 0):\n", i+1, getVertex(graph, i+1), j);
	    	scanf("%d", &next_vertex);
	    	//采用头插法进行链表的连接 
	    	if(next_vertex != 0)
	    	{
	    		q = (ArcNode *)malloc(NODE);
	    		puts("请输入它们所形成弧的权值:");
	    		scanf("%d", &q->length);
	    		q->adjvex = next_vertex;
	    		q->next = graph->vertex[i].firstarc;
	    		graph->vertex[i].firstarc = q;
			}
			else break;
			j++;
		}while(1);
	}
}


//销毁图 G
AdjList destoryGraph(AdjList graph)
{
	free(graph);
	graph = NULL;
	return graph;
}


//若图中存在顶点 v , 则返回顶点 v在图 G 中的位置。若无, 返回 0 
int locateVertex(AdjList graph, TypeElem data)
{
	int i;
	for(i = 0; i < graph->vexnum; i++)
	{
		if(graph->vertex[i].data == data) break;
	}
	if(i == graph->vexnum) return 0;
	else return i+1;
}

//根据序号取得顶点的值 
TypeElem getVertex(AdjList graph, int i)
{
	return graph->vertex[i-1].data;
}


//返回图 G 中顶点 v 的第一个邻接点。若 v 无邻接点或图 G 中无顶点 v ,则函数返回值为 NULL 
TypeElem firstAdjVertex(AdjList graph, TypeElem data)
{
	int i = locateVertex(graph, data);
	ArcNode *p;
	if(i == 0)
	{
		puts("此图中无此顶点, 所以无法找到它的邻接点, 请重试!");
		return NULL;
	}
	if(graph->vertex[i-1].firstarc == NULL)
	{
		puts("此图中该顶点无邻接点, 请重试!");
		return NULL;
	}
	p = graph->vertex[i-1].firstarc;
	while(p->next != NULL) p = p->next;		//由于存储弧结点的链表采用头插法, 所以最后一个结点即为第一个邻接点 
	return getVertex(graph, p->adjvex);
}


//返回顶点 v 的下一个邻接点(紧跟在w 后边)。若 w是 v 的最后一个邻接点, 则函数返回 NULL 
TypeElem nextAdjVertex(AdjList graph, TypeElem v, TypeElem w)
{
    int i = locateVertex(graph, v);
    int j = locateVertex(graph, w);
    ArcNode *p;
	if(i == 0)
	{
		puts("此图中无此顶点, 所以无法找到它的邻接点, 请重试!");
		return NULL;
	}	
	if(graph->vertex[i-1].firstarc == NULL)
	{
		puts("此图中该顶点无邻接点, 请重试!");
		return NULL;
	}
	p = graph->vertex[i-1].firstarc;
	while(p->next->adjvex != j) p = p->next;
	if(p != graph->vertex[i-1].firstarc)	//说明已经成功找到v的邻接点w后的第一个邻接点 
	return getVertex(graph, p->adjvex);
	else									//说明未找到
	return NULL; 
}


//在图 G 中增加一个顶点 u 
void insertVertex(AdjList graph, TypeElem u)
{
	graph->vertex[graph->vexnum].data = u;
	graph->vertex[graph->vexnum].firstarc = NULL;
	graph->vexnum++;
}


//删除图 G 的顶点 v及与顶点 v相关联的弧 
void deleteVertex(AdjList graph, TypeElem v)
{
	int i = locateVertex(graph, v)-1;
	for(int j = i; j < graph->vexnum-1; j++)		//删除图 G 的顶点 v 
	graph->vertex[j] = graph->vertex[j+1];
	graph->vexnum--;
	for(int i = 0; i < graph->vexnum; i++)			//删除所有与图 G 顶点 v有关的所有弧 
	{
		ArcNode *p = (ArcNode *)malloc(sizeof(ArcNode));
		p->next = graph->vertex[i].firstarc;
		while(p->next != NULL)
		{
			if(p->next->adjvex == i+1)
			{
				p->next = p->next->next;
			}
			p = p->next;
		}
	}
}


//在图 G 中增加一条从顶点 v 到顶点 w 的弧 
void insertArc(AdjList graph, TypeElem v, TypeElem w)
{
	int i = locateVertex(graph, v);
	int j = locateVertex(graph, w);
	ArcNode *p;
	p = (ArcNode *)malloc(sizeof(ArcNode));
	p->adjvex = j;
	p->next = graph->vertex[i-1].firstarc->next;
	graph->vertex[i-1].firstarc->next = p;
	printf("插入顶点 %c 到 %c 的弧成功!\n", v, w);
}


//删除图 G 中从顶点 v 到顶点 w 的弧 
void deleteArc(AdjList graph, TypeElem v, TypeElem w)
{
	int i = locateVertex(graph, v);
	int j = locateVertex(graph, w);
	ArcNode *p = (ArcNode *)malloc(sizeof(ArcNode));
	p->next = graph->vertex[i-1].firstarc;
	while(p->next != NULL)			//删除以 顶点 v为起始点, 顶点 w 为终止点的弧 
	{
		if(p->next->adjvex == j) p->next = p->next->next;
		p = p->next;
	}
	p->next = graph->vertex[j-1].firstarc;
	while(p->next != NULL)			//删除以 顶点 w为起始点, 顶点 v 为终止点的弧 
	{
		if(p->next->adjvex == i) p->next = p->next->next;
		p = p->next;
	}
	printf("删除从顶点 %c 到顶点 %c 的弧成功!\n", v, w);
}



//打印图的邻接表 
void printGraph(AdjList graph)
{
	printf("此图的邻接表如下:\n\n");
    for(int i = 0; i < graph->vexnum; i++)
	{
		ArcNode *p = graph->vertex[i].firstarc;
	    printf("%c-->", graph->vertex[i].data);
		while(p != NULL)
		{
			printf("%d(weight = %d)-->", p->adjvex, p->length);
			p = p->next;
		}
		printf("\n");
	}
}
